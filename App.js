
import React, { Component } from 'react';
import RootNav from './src/navigations/RootNav';
import { Provider } from 'react-redux';
import store from './src/redux/configureStore';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootNav/>
      </Provider>
        
    );
  }
}


