import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PanResponder
} from 'react-native';
import DatePicker from 'react-native-datepicker';

class CustomDatePicker extends Component {

  constructor(props) {
    super(props);

    this.state = {
      date: ''
    };
  }

  // componentWillMount() {
  //   this._panResponder = PanResponder.create({
  //     onStartShouldSetPanResponder: (e) => {console.log('onStartShouldSetPanResponder'); return true;},
  //     onMoveShouldSetPanResponder: (e) => {console.log('onMoveShouldSetPanResponder'); return true;},
  //     onPanResponderGrant: (e) => console.log('onPanResponderGrant'),
  //     onPanResponderMove: (e) => console.log('onPanResponderMove'),
  //     onPanResponderRelease: (e) => console.log('onPanResponderRelease'),
  //     onPanResponderTerminate: (e) => console.log('onPanResponderTerminate')
  //   });
  // }

  render() {
    return (
      <DatePicker
        style={[{ width: '40%' }, this.props.style]}
        showIcon={this.props.showIcon}
        date={this.state.date}
        mode="date"
        placeholder={this.props.title}
        format='YYYY-MM-DD'
        confirmBtnText="Chọn"
        cancelBtnText="Huỷ"
        onDateChange={(date) => {
          this.setState({ date: date });
          this.props.getValue(date);
        }
        }
      />

    );
  }
}

const styles = StyleSheet.create({

});

export default CustomDatePicker;

/**
 *<Text style={styles.instructions}>date: {this.state.date}</Text>
        <DatePicker
          style={{width: 200}}
          date={this.state.time}
          mode="time"
          format="HH:mm"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          minuteInterval={10}
          onDateChange={(time) => {this.setState({time: time});}}
        />
       <Text style={styles.instructions}>time: {this.state.time}</Text>
        <DatePicker
          style={{width: 200}}
          date={this.state.datetime}
          mode="datetime"
          format="YYYY-MM-DD HH:mm"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          showIcon={false}
          onDateChange={(datetime) => {this.setState({datetime: datetime});}}
        />
        <Text style={styles.instructions}>datetime: {this.state.datetime}</Text>
        <DatePicker
          style={{width: 200}}
          date={this.state.datetime1}
          mode="datetime"
          format="YYYY-MM-DD HH:mm"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          minuteInterval={10}
          onDateChange={(datetime) => {this.setState({datetime1: datetime});}}
        />
        <Text style={styles.instructions}>datetime: {this.state.datetime1}</Text>
 */
