//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet ,ActivityIndicator } from 'react-native';

// create a component
export default class Loading extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator
                    style={{flex:1}}
                    color={'#0099ff'}
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'white'
    },
});

//make this component available to the app
