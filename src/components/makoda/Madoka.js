import React from 'react';
import PropTypes from 'prop-types';
import {
    Animated,
    TextInput,
    TouchableWithoutFeedback,
    View,
    StyleSheet,
} from 'react-native';

import BaseInput from './BaseInput';

const LABEL_HEIGHT = 24;
const PADDING = 12;

export default class Madoka extends BaseInput {
    static propTypes = {
        /*
        * this is applied as active border and label color
        */
        borderColor: PropTypes.string,
        height: PropTypes.number,
        noteType: PropTypes.bool,
    };

    static defaultProps = {
        borderColor: '#7A7593',
        animationDuration: 250,
        height: 48,
        noteType: false
    };

    render() {
        const {
            label,
            style: containerStyle,
            height: inputHeight,
            inputStyle,
            labelStyle,
            borderColor,
            note,
            noteStyle,
            noteType
 } = this.props;
        const {
            width,
            focusedAnim,
            value,

 } = this.state;

        return (
            <View
                style={[containerStyle, { height: inputHeight + LABEL_HEIGHT + 8 }]}
                onLayout={this._onLayout}
            >
                <View
                    style={[styles.inputContainer, { borderBottomColor: borderColor }]}
                >
                    <TextInput
                        ref="input"
                        {...this.props}
                        style={[
                            styles.textInput,
                            inputStyle,
                            {
                                width,
                                height: inputHeight,
                                //backgroundColor:'green'
                            },
                        ]}
                        value={value}
                        onBlur={this._onBlur}
                        onChange={this._onChange}
                        onFocus={this._onFocus}
                        underlineColorAndroid={'transparent'}
                        
                        
                    />

                    <Animated.View
                        style={{
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                            width: 1,
                            height: focusedAnim.interpolate({
                                inputRange: [0, 0.2, 1],
                                outputRange: [0, inputHeight, inputHeight],
                            }),
                            backgroundColor: borderColor,
                        }}
                    />

                    <Animated.View
                        style={{
                            position: 'absolute',
                            right: 0,
                            top: 0,
                            height: 1,
                            width: focusedAnim.interpolate({
                                inputRange: [0, 0.2, 0.8, 1],
                                outputRange: [0, 0, width, width],
                            }),
                            backgroundColor: borderColor,
                        }}
                    />

                    <Animated.View
                        style={{
                            position: 'absolute',
                            left: 0,
                            top: 0,
                            width: 1,
                            height: focusedAnim.interpolate({
                                inputRange: [0, 0.8, 1],
                                outputRange: [0, 0, inputHeight],
                            }),
                            backgroundColor: borderColor,
                        }}
                    />
                </View>
                <TouchableWithoutFeedback onPress={this.focus}>
                    <Animated.View
                        style={[
                            styles.labelContainer,
                            {
                                width,
                                height: LABEL_HEIGHT,
                                top: focusedAnim.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [LABEL_HEIGHT + PADDING, 12],
                                }),
                            },
                        ]}
                    >
                        <Animated.Text
                            style={[
                                styles.label,
                                labelStyle,
                                {
                                    fontSize: focusedAnim.interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [18, 14],
                                    }),
                                },
                            ]}
                        >
                            {label}
                        </Animated.Text>
                        {
                            this.state.noteType && <Animated.Text
                                style={[
                                    styles.label,
                                    noteStyle,
                                    {
                                        fontSize: focusedAnim.interpolate({
                                            inputRange: [0, 1],
                                            outputRange: [18, 14],
                                        }),
                                    },
                                ]}
                            >
                                {note}
                            </Animated.Text>
                        }
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        borderBottomWidth: 1,
        position: 'absolute',
        bottom: 0
    },
    labelContainer: {
        position: 'absolute',
        left: PADDING,
        top: LABEL_HEIGHT + PADDING,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 16
    },
    label: {
        backgroundColor: 'transparent',
        fontFamily: 'Arial',
        color: '#6a7989',
    },
    textInput: {
        paddingHorizontal: PADDING,
        paddingVertical: 0,
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
    },
});