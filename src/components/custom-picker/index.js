import React, { Component } from "react";
import { View, Platform, Dimensions, Text } from "react-native";
import { Picker, Form, Item as FormItem } from "native-base";
const Item = Picker.Item;
const screen = Dimensions.get('window')
import color from '../../ultis/styles/common-css';
export default class PickerIOS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: -1
    };
  }
  onValueChange(value) {
    this.setState({
      selected: value
    });
    this.props.getAddressCity(value);
  }
  render() {
    return (
      <Form>
         {
          (Platform.OS === 'android') ? <Text style={{ marginLeft: 5 }}>{this.props.placeholder}</Text> : null
        } 
        <Picker
          {...this.props}
          iosHeader={this.props.iosHeader}
          placeholder={this.props.placeholder}
          note={false}
          selectedValue={this.state.selected}
          onValueChange={(itemValue) => this.onValueChange(itemValue)}
          style={[styles.pickerIos, this.props.style]}
          mode={this.props.mode}
        >
          {
            this.props.data.map((item, i) => (
              <Item label={item.name} value={item.id} key={item.id} color={this.props.color} />
            ))
          }
        </Picker>
      </Form>
    );
  }
}
const styles = {
  pickerAndroid: {
    borderColor: color.colorBlue,
    width: screen.width - 40,
    
    backgroundColor: color.colorGray,
    borderBottomWidth: 2,
  },
  pickerIos: {
    borderBottomWidth: 2,
    borderColor: color.colorBlue,
    width: screen.width - 20,
    //backgroundColor: color.colorGray
  }
}
