//import liraries
import React, { Component } from 'react';
import { View, Text, Modal } from 'react-native';
// create a component
export default class ModalDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isShow: false,
            textContent: '',
        }
    }
    _setShow(visible, textContent) {
        this.setState({
            isShow: true,
            textContent: textContent
        });
        setTimeout(() => {
            this.setState({
                isShow: false
            })
        }, 1500)
    }
    render() {
        return (
            <Modal
                animationType={"none"}
                transparent={true}
                visible={this.state.isShow}
                onRequestClose={() => this._setShow(false)}
                style={{ flex: 1 }}
            >
                <View style={styles.outsideContainer} />
                <View style={styles.viewText}>
                    <Text style={styles.textContent}>{this.state.textContent}</Text>
                </View>

            </Modal>
        );
    }
}
// define your styles
const styles = {
    outsideContainer: {
        backgroundColor: '#000000',
        width: 200,
        height: 40,
        opacity: 0.6,
        position: 'absolute',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        top: 50,
    },
    textContent: {
        color: 'white',
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    viewText: {
        backgroundColor: 'transparent',
        width: 200,
        height: 40,
        position: 'absolute',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        top: 50,
    }
};

//make this component available to the app
