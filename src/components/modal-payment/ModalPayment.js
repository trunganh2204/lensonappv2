import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, TextInput, Modal, Image, ScrollView } from 'react-native';
import { Text } from 'native-base';
import color from '../../ultis/styles/common-css';
import CustomTextInput from '../text-input';
import BasicView from '../../ultis/styles/AppStyles';
class ModalPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShow: false,
        }
    }
    _setShow(visible) {
        this.setState({
            isShow: visible,
        });
    }
    render() {
        return (
            <Modal
                style={styles.container}
                animationType={"slide"}
                visible={this.state.isShow}
                transparent={true}
                onRequestClose={() => this._setShow(false)}
            >
                <View style={styles.outsideContainer} />

                <ScrollView style={styles.content}>
                    <View style={styles.header}>
                        <Text style={styles.textTitle}>Thanh toán</Text>
                        <TouchableOpacity
                            style={styles.onPressDelete}
                            onPress={() => this._setShow(false)}
                        >
                            <Image
                                source={require('../../../img/delete.png')}
                                style={styles.iconDelete}
                                resizeMode={'center'}
                            />
                        </TouchableOpacity>
                    </View>

                    <CustomTextInput
                        placeholder={'Họ và tên'}
                        style={styles.textInput}
                        textTitle={'Họ và tên'}
                    />
                    <CustomTextInput
                        placeholder={'Số tài khoản'}
                        style={styles.textInput}
                        textTitle={'Số tài khoản'}
                    />
                    <CustomTextInput
                        placeholder={'Ngân hàng - Chi nhánh'}
                        style={styles.textInput}
                        textTitle={'Ngân hàng - Chi nhánh'}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <CustomTextInput
                            placeholder={'Ngày thanh toán'}
                            styleParent={{
                                width: '45%',
                            }}
                            style={{ width: '90%' }}
                            styleTitle={{ marginLeft: '10%' }}
                            textTitle={'Ngày thanh toán'}
                        />
                        <CustomTextInput
                            placeholder={'Số tiền'}
                            styleParent={{
                                width: '45%',
                            }}
                            style={{ width: '90%' }}
                            styleTitle={{ marginLeft: '10%' }}
                            textTitle={'Số tiền'}
                        />
                    </View>

                    <CustomTextInput
                        placeholder={'Ghi chú'}
                        style={styles.textInput}
                        textTitle={'Ghi chú'}
                    />
                    <View style={[BasicView.horizontalView, { justifyContent: 'center', alignItems: 'center', marginTop: 30 }]}>
                        <TouchableOpacity
                            style={[styles.buttonContainer, { backgroundColor: color.activeButtonColor, marginRight: '4%' }]}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Text style={styles.textTitle}>Huỷ bỏ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.buttonContainer, { backgroundColor: color.colorBlue }]}>
                            <Text style={styles.textTitle}>Xác nhận</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </Modal>
        );
    }
}
const screen = Dimensions.get('window');
const numberRadius = 5;
const styles = {
    container: {
        width: screen.width,
        height: screen.height,
        justifyContent: 'center',
        alignItems: 'center'
    },
    outsideContainer: {
        flex: 1,
        backgroundColor: '#000000',
        width: screen.width,
        opacity: 0.6,
        position: 'absolute',
        height: screen.height
    },
    header: {
        height: 50,
        width: '100%',
        backgroundColor: color.colorBlue,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: numberRadius,
        borderTopRightRadius: numberRadius,
        marginBottom: 5
    },
    textTitle: {
        color: color.colorWhite,
        fontSize: 15,
    },
    buttonContainer: {
        height: 40,
        width: '43%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    content: {
        width: '90%',
        height: 100,
        backgroundColor: 'white',
        borderRadius: numberRadius,
        alignSelf: 'center',
        marginTop: 30,
        marginBottom: 30
    },
    onPressDelete: {
        position: 'absolute',
        right: 0,
        width: 50,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconDelete: {
        width: 15,
        height: 15,
    },
    textInput: {
        width: '90%',
        alignSelf: 'center'
    }
}
export default ModalPayment;