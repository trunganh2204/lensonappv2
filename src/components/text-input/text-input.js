import React, { Component } from 'react';
import { View, TextInput, Dimensions, TouchableOpacity, Image, Text, Platform } from 'react-native';
import Madoka from '../makoda/Madoka';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import CustomPicker from '../custom-picker';
import color from '../../ultis/styles/common-css';
const screen = Dimensions.get('window')
import dataProvince from '../../ultis/api/local-data/province.json';
class MyTextInput extends Component {
    constructor(props) {
        super(props)
        this.state = {
            valuePicker: '',
            
        }
    }

    render() {
        const {
            onEnter, onChange,
            placeholder, returnKeyType,
            inputRef, keyboardType,
            style, label, labelStyle,
            inputStyle, editable, onBlur
            } = this.props
        return (
            <View style={[styles.card1, { backgroundColor: color.colorGray }]}>
                {
                    (this.props.textTitle)
                        ?
                        <Text style={[styles.textTitle, { ...this.props.styleTitle }]}>{this.props.textTitle}</Text>
                        :
                        null
                }
                <Madoka
                    {...this.props}
                    value={this.props.value}
                    iconClass={FontAwesomeIcon}
                    label={label}
                    labelStyle={[{
                        color: 'black',
                        fontWeight: 'normal'
                    }, labelStyle]}
                    borderColor={color.colorBlue}
                    ref={(ref) => this.text = ref}
                    inputStyle={[{
                        color: 'black',
                        fontWeight: 'normal'
                    }, inputStyle]}
                    style={[styles.input, this.props.style]}
                    underlineColorAndroid={'transparent'}
                    keyboardType={keyboardType}
                    returnKeyType={returnKeyType}
                    onSubmitEditing={(e) => {
                        if (this.props.onSubmitEditing === undefined) return;
                        this.props.onSubmitEditing(e.nativeEvent.text);

                    }}
                    onChange={(e) => {
                        if (onChange === undefined) return;
                        onChange(e.nativeEvent.text)
                    }}
                    note={this.props.note}
                    editable={editable}
                    // onBlur={(e) => {
                    //     if (this.props.onBlur === undefined) return;
                    //     this.props.onBlur(e.nativeEvent.text)
                    // }}
                    onFocus={(e) => {
                        if(this.props.onFocus === undefined) return;
                        this.props.onFocus(e.nativeEvent.text)
                    }}

                />
                {
                    (this.props.imageIcon) ?
                        <TouchableOpacity
                            onPress={this.props.onPressIcon}
                            style={styles.onPressIcon}
                        >
                            <Image
                                style={styles.icon}
                                source={this.props.source}
                                resizeMode={'contain'}
                            />
                        </TouchableOpacity>
                        :
                        null
                }
                { 
                    (this.props.picker) ?
                        <CustomPicker
                            style={(Platform.OS === 'android') ? styles.onPressIcon3 : styles.onPressIcon2}
                            getAddressCity={(value) => {
                                this.setState({valuePicker: value });
                                if(this.props.checkPickerSelected !== undefined) {
                                    this.props.checkPickerSelected(value);
                                }
                                
                            }}
                            data={dataProvince}
                            placeholder={(Platform.OS === 'ios') ? 'Tỉnh thành...' : null}
                        />
                        :
                        null
                }
            </View>

        )
    }
}
const styles = {
    container: {
        height: 40,
        width: screen.width - 20,
        borderWidth: 1,
        backgroundColor: color.colorGray,
        borderRadius: 5,
        margin: 5,
        padding: 10
    },
    card1: {
        // paddingVertical: 16,
        //marginLeft: 10
    },
    textTitle: {
        color: 'black',
        marginLeft: 30
    },
    icon: {
        width: 27,
        height: 27,
        alignSelf: 'center'
    },
    onPressIcon: {
        position: 'absolute',
        right: 20,
        top: 42,
        width: 30,
        height: 30,
         
    },
     onPressIcon2: {
        position: 'absolute',
        right: -30,
        top: -60,
        width: 30,
        height: 30,
        borderBottomWidth: 0,
        borderColor: color.colorBlue,
        width: screen.width / 2,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginRight: 15
       // backgroundColor: 'red'
    },
    onPressIcon3: {
        position: 'absolute',
        right: -70,
        top: -57,
        width: 30,
        height: 30,
        borderBottomWidth: 0,
        borderColor: color.colorBlue,
        width: screen.width / 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 15
       // backgroundColor: 'red'
    },
    input: {
        backgroundColor: color.colorGray,
        width: screen.width - 40,
        marginBottom: 10

    }

}
export default MyTextInput;