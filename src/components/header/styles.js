const React = require("react-native");
import '../../ultis/styles/common-css';
import { Dimensions } from 'react-native';
const screen = Dimensions.get('window')
export default {
  container: {
    width: '100%',
    flexDirection: 'row'
  },
  textTitle: {
    color: 'black',
    fontSize: 17,

  },
  header: {
    alignItems: 'center',
    backgroundColor: "#fff",
    height: 50,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    width: screen.width,
    justifyContent: 'center'
  },
  iconLeftHeader: {
    fontSize: 30,
    color: '#4493AA',
  },
  buttonLeft: {
    position: 'absolute',
    left: 0,
    width: 50,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonRight: {
    position: 'absolute',
    right: 0,
    width: 50,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconRightHeader: {
    fontSize: 30,
    color: '#4493AA',
  },
  icon: {
    fontSize: 25,
    color: 'gray'
  }
};

