import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
class CardActiveWarranty extends Component {
    state = {}
    _renderActive() {
        return (
            <List>
                <ListItem >
                    <Body>
                        <Text>Khách hàng</Text>

                    </Body>
                    <Body>
                        <Text >Kiên/ 0969 995 997</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Model</Text>

                    </Body>
                    <Body>
                        <Text >HJ-19TL12</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Serial cục nóng</Text>

                    </Body>
                    <Body>
                        <Text >1234567</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Serial cục lạnh</Text>

                    </Body>
                    <Body>
                        <Text >7654321</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Ngày kích hoạt</Text>

                    </Body>
                    <Body>
                        <Text >01/10/2017</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Mã kích hoạt</Text>

                    </Body>
                    <Body>
                        <Text >MKH-1234</Text>
                    </Body>
                </ListItem>
            </List>
        );
    }
    _renderWarranty() {
        return (
            <List>
                <ListItem >
                    <Body>
                        <Text>Khách hàng</Text>

                    </Body>
                    <Body>
                        <Text >Kiên/ 0969 995 997</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Model</Text>

                    </Body>
                    <Body>
                        <Text >HJ-19TL12</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Serial cục nóng</Text>

                    </Body>
                    <Body>
                        <Text >1234567</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Serial cục lạnh</Text>

                    </Body>
                    <Body>
                        <Text >7654321</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Ngày kích hoạt</Text>

                    </Body>
                    <Body>
                        <Text >01/10/2017</Text>
                    </Body>
                </ListItem>
                <ListItem >
                    <Body>
                        <Text>Mã kích hoạt</Text>

                    </Body>
                    <Body>
                        <Text >MKH-1234</Text>
                    </Body>
                </ListItem>
            </List>
        );
    }
    render() {
        return (
            <Content style={styles.container}>
                {this.props.renderActive ? this._renderActive() : this._renderWarranty()}
            </Content>
        );
    }
}

const screen = Dimensions.get('window')
const styles = {
    container: {
        borderBottomWidth: 1,
        margin: 10
    }
}
export default CardActiveWarranty;