import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, TextInput, Modal, Image, ScrollView } from 'react-native';
import { Text } from 'native-base';
import color from '../../ultis/styles/common-css';
import Form from '../text-input/form';
import CustomTextInput from '../text-input/text-input';
import Loading from '../../components/loading/Loading';
import BasicView from '../../ultis/styles/AppStyles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomPicker from '../../components/custom-picker';
import Customer from './CustomerController';
import dataProvince from '../../ultis/api/local-data/province.json';
class ModalCreateUser extends Component {
    constructor(props) {
        super(props);
        customer = new Customer(); 
        this.state = {
            isLoading: false,
            isShow: false,
            accText: '',
            fullNameText: '',
            textEmail: '',
            textPhoneNum: '',
            labelCustomerName: '',
            labelPhoneNum: '',
            noteCustomer: 'Ví dụ: Nguyễn Văn A',
            noteAccount: 'Nếu tài khoản đã tồn tại, hãy sửa lại...',
            notePhoneNum: "Ví dụ: 0987654321",
            noteEmail: "Ví dụ: 60-Bà Triệu-Hai Bà Trưng-HN",
            resultCheck: false,
            mess: ''
        }
    }
    _setShow(visible) {
        this.setState({
            isShow: visible,
        });
    }
    _handleNextInput(inputRef) {
        this.refs[inputRef].text.focus()
    }
    _checkNull() {
        const { accText, fullNameText, textPhoneNum, textEmail } = this.state
        if (accText === '' || fullNameText === '' || textPhoneNum === '' || textEmail === '') {
            this.setState({ mess: 'Thiếu thông tin khách hàng...' })
        }
        else {
            customer.sendRequestCreateCustomer(customer, this)
           

        }
    }
    render() {

        return (
            <Modal
                style={styles.container}
                animationType={"slide"}
                visible={this.state.isShow}
                transparent={true}
                onRequestClose={() => this._setShow(false)}
            >
                <KeyboardAwareScrollView
                    style={{ flex: 1 }}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    scrollEnabled={true}
                >

                  

                    <View style={styles.content}>
                        <View style={styles.header}>
                            <Text style={styles.textTitle}>Tạo mới khách hàng</Text>
                            <TouchableOpacity
                                style={styles.onPressDelete}
                                onPress={() => this._setShow(false)}
                            >
                                <Image
                                    source={require('../../../img/delete.png')}
                                    style={styles.iconDelete}
                                    resizeMode={'center'}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ backgroundColor: color.colorGray }}>
                            <CustomTextInput
                                label={'Tên khách hàng'}
                                note={this.state.noteCustomer}
                                value={this.state.fullNameText}
                                style={styles.textInput}
                                returnKeyType={'next'}
                                ref='1'
                                onChange={(text) => { customer.setFullName(text); this.setState({ fullNameText: text }); customer.toggleAccountName(text, this) }}
                                onSubmitEditing={(text) => { customer.toggleAccountName(text, this); this.setState({ fullNameText: text }); this._handleNextInput(2) }}
                            />
                            <CustomTextInput
                                value={this.state.accText}
                                label={'Tài khoản'}
                                ref='2'
                                style={styles.textInput}
                                returnKeyType={'next'}
                                onChange={(text) => { customer.setFullName(text) }}
                                onSubmitEditing={() => { }}
                            />


                            <CustomTextInput
                                label={'Số điện thoại'}
                                value={this.state.textPhoneNum}
                                note={this.state.notePhoneNum}
                                style={styles.textInput}
                                returnKeyType={'next'}
                                keyboardType={'numeric'}
                                ref="3"
                                onChange={(text) => { customer.setPhoneNum(text); this.setState({ textPhoneNum: text }); }}
                                onSubmitEditing={(text) => { this._handleNextInput(3) }}
                            />
                            <CustomTextInput
                                value={this.state.textEmail}
                                label={'Địa chỉ'}
                                note={this.state.noteEmail}
                                style={styles.textInput}
                                onChange={(text) => { customer.setEmail(text); this.setState({ textEmail: text }) }}
                                returnKeyType={'next'}
                                ref="4"
                                onSubmitEditing={() => { }}
                            />

                            <CustomPicker
                                getAddressCity={(value) => console.log(value)}
                                data={dataProvince}
                                placeholder={'Tỉnh/Thành phố....'}
                            />

                            {this.state.isLoading && <Loading />}
                            <Text style={{ alignSelf: 'center', marginTop: 20 }}>{this.state.mess}</Text>
                            <TouchableOpacity
                                style={[styles.buttonContainer, { backgroundColor: color.colorBlue }]}
                                onPress={async () => {
                                    await this._checkNull()

                                }}
                            >
                                <Text style={styles.textTitle}>Tạo mới</Text>
                            </TouchableOpacity>
                            <View style={{ width: screen.width, height: 30, backgroundColor: color.colorGray }}></View>
                        </View>


                    </View>


                </KeyboardAwareScrollView>
            </Modal>
        );
    }
}
const screen = Dimensions.get('window');
const numberRadius = 5;
const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    outsideContainer: {
        flex: 1,
        backgroundColor: '#000000',
        width: screen.width,
        opacity: 0.6,
        position: 'absolute',
        height: screen.height
    },
    header: {
        height: 50,
        width: '100%',
        backgroundColor: color.colorBlue,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: numberRadius,
        borderTopRightRadius: numberRadius,

    },
    textTitle: {
        color: color.colorWhite,
        fontSize: 15,
    },
    buttonContainer: {
        height: 40,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        alignSelf: 'center',
        marginBottom: 10,
        marginTop: 10,
        backgroundColor: color.colorGray
    },
    content: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        borderRadius: numberRadius,
        alignSelf: 'center',
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    onPressDelete: {
        position: 'absolute',
        right: 0,
        width: 50,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',

    },
    iconDelete: {
        width: 15,
        height: 15,
    },
    textInput: {
        width: screen.width - 20,
        height: 20,
        alignSelf: 'center'
    }
}
export default ModalCreateUser;

/**
 *
 */