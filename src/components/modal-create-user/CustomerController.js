
export default class Customer {
  // Define Properties...
  _fullName = '';
  _account = '';
  _phoneNum = '';
  _email = '';
  _adress = '';
  _city = '';
  _isLoading = true;
  _response = {};
  // Define method.....
  setAccount(account) {
    this._account = account
  }
  setResponse(response) {
    this._response = response;
  }
  setFullName(fullName) {
    this._fullName = fullName;
  };
  setPhoneNum(phoneNum) {
    this._phoneNum = phoneNum;
  };
  setEmail(email) {
    this._email = email;
  };
  setAdress(adress) {
    this._adress = adress;
  };
  setCity(city) {
    this._city = city;
  };
  getFullName() {
    return this._fullName;
  };
  getPhoneNum() {
    return this._phoneNum;
  };
  getEmail() {
    return this._email;
  };
  getAdress() {
    return this._adress;
  };
  getCity() {
    return this._city;
  };
  getResponse() {
    return this._response;
  };
  getAccount() {
    return this._account;
  };
  checkInvalidPhoneNum(phoneNum) {
    if (!(phoneNum.match("[0-9]+"))) return false;
    if (phoneNum.startsWith("09") && phoneNum.length == 10) return true;
    if (phoneNum.startsWith("01") && phoneNum.length == 11) return true;
    if (phoneNum.startsWith("849") && phoneNum.length == 11) return true;
    if (phoneNum.startsWith("841") && phoneNum.length == 12) return true;
    return false;
  };
  generalAccountCustomer(string) {
    var temp = "";
    string = '' + string;
    splitstring = string.split(" ");
    for (i = 0; i < splitstring.length; i++)
      temp += splitstring[i];
    return temp;

  }
  convertFullName(value) {
    var str;
    str = value;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    //str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");  
    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
    //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-  
    str = str.replace(/^\-+|\-+$/g, "");
    //cắt bỏ ký tự - ở đầu và cuối chuỗi 
    let result = this.generalAccountCustomer(str)
    this.setAccount(result)
   return result;
  }
  toggleAccountName(text, cb){
    cb.setState({
      accText: this.convertFullName(text)
    })
  }
  async   sendRequestCreateCustomer(customer, cb) {
    cb.setState({ isLoading: true })
    await fetch('https://app-dot-casper-electric.appspot.com/api?action=signUp', {

      method: 'POST',
      body: JSON.stringify({

        fullName: customer.getFullName(),
        account: this.convertFullName(customer.getFullName()),
        email: customer.getEmail(),
        phoneNum: customer.getPhoneNum(),
        adress: customer.getAdress(),
        city: customer.getCity()

      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then((responJSON) => {
        
        console.log('ket qua setResonpse', this.getResponse())
        if(responJSON.data.loginCode ==1) {
          cb.setState({ resultCheck : false, isLoading: false, mess: 'Tài khoản đã đăng kí , chọn tài khoản khác'})
          return
        }
        else{
          this.setResponse(responJSON.data);
          cb.setState({ isLoading: false, 
            accText: '', 
            mess: '', 
            resultCheck: true,
            fullNameText: '',
            textEmail: '',
            textPhoneNum: '',
            titleBankAcc: 'Tên chủ tài khoản ',
            titleBankNum: 'Số tài khoản',
            bankNum: '',
            ownerAcc: '',
            labelCustomerName: '',
            labelPhoneNum: '',
            labelSerial: '',
            lableSerial2: '',
            labelActiveCode: '', })
            cb.props.aaa(customer.getResponse())
            cb._setShow(false) 
          return
        }
        
      })
      .catch((error) => { console.log(error) })
      ;

  };

}

