import { StackNavigator } from 'react-navigation';
// Import Unauthorize StackNavigator
import { Root } from "native-base";
import React, { Component } from 'react';
import Active from '../screens/authorized/active/Active';
import Payment from '../screens/authorized/payment/drawer-payment/paymentNav';
import Splash from '../screens/unauthorized/splash-screen/Splash';
import Login from '../screens/unauthorized/login/Login';
import AccountInfo from '../screens/unauthorized/register/account-info/index';
import UserInfo from '../screens/unauthorized/register/user-info/index';
import ForgotPassword from '../screens/unauthorized/forgot-password/ForgotPassword';
import Home from '../screens/home/Home';
import ChangePassword from '../screens/authorized/change-password/ChangePassword';

const RootNav = StackNavigator({
    Splash: {
        screen: Splash,
        navigationOptions: {
            header: null
        }
    },
    ForgotPassword: {
        screen: ForgotPassword,
        navigationOptions: {
            header: null
        }
    },
    Home: {
        screen: Home,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    AccountInfo: {
        screen: AccountInfo,
        navigationOptions: {
            header: null
        }
    },
    UserInfo: {
        screen: UserInfo,
        navigationOptions: {
            header: null
        }
    },
    Payment: {
        screen: Payment,
        navigationOptions: {
            header: null
        }
    },
    Active: {
        screen: Active,
        navigationOptions: {
            header: null
        }
    },
    ChangePassword: {
        screen: ChangePassword,
        navigationOptions: {
            header: null
        }
    }

},
    {
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;


                const translateX = position.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [layout.initWidth, 0, 0],
                });

                const opacity = position.interpolate({
                    inputRange: [index - 1, index - 0.99, index],
                    outputRange: [0, 1, 1],
                });

                return { opacity, transform: [{ translateX }] };
            },
            isModal: false
        }),
        headerMode: 'none'

    }
);
export default () =>
    <Root>
        <RootNav />
    </Root>;

//export default RootNav;
