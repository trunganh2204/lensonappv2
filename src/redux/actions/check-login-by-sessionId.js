import {CHECK_LOGIN_BY_SESSIONID} from './types';

export function checkLoginBySessionId(data){
    return{
        type: CHECK_LOGIN_BY_SESSIONID,
        data
    }
}