import { CREATE_USER, CREATE_USER_SUCCESS, CREATE_USER_FAILURE } from './types';

export function createUser() {
    return {
        type: CREATE_USER
    }
}
export function createUserSuccess(data) {
    return {
        type: CREATE_USER_SUCCESS,
        data
    }
}

export function createUserFailure() {
    return {
        type: CREATE_USER_FAILURE
    }
}

export function createNewUser(userInfo) {
    return dispatch =>
    fetch(
        `http://api-dot-casper-electric.appspot.com/api?action=signup`,        {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            fullName: userInfo.fullName,
            password: userInfo.password,
            account: userInfo.account,
            phoneNum: userInfo.phoneNum,
            bankName: userInfo.bankName,
            bankNum: userInfo.bankNum,
            addressStreet: userInfo.addressStreet,
            addressCity: userInfo.addressCity,
            addressDistrict: userInfo.addressDistrict,
        }),
    })
    .then(response => {
        if(response.status >= 200 && response.status < 300) {
            dispatch(createUserSuccess(response));
        } else {
            const error = new Error(response.statusText);
            error.response = response;
            dispatch(createUserFailure(error));
            throw error;
        }
    })
    .catch(error => {console.log('REQUEST FAILEDDD CMNR',error)});
    
}