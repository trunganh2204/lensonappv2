import {GET_ONLINE} from './types';

export function getOnline(data){
    return{
        type: GET_ONLINE,
        data
    }
}