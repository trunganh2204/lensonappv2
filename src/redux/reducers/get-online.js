import { GET_ONLINE } from '../actions/types';
const initialState = {
    data: false,
}
export default function getOnline(state = initialState, action) {
    switch (action.type) {
        case GET_ONLINE:
            return {
                data: action.data,
            }
        default:
            return state
    }
}