import { CREATE_USER, CREATE_USER_SUCCESS, CREATE_USER_FAILURE} from '../actions/types';
const initialState = {
  data: [],
  isFetching: false,
  error: false,
  isFetched: false
}

export default function productInCategory (state = initialState, action) {
  switch (action.type) {
    case CREATE_USER:
      return {
        ...state,
        data: [],
        isFetching: true,
        isFetched: false,
      }
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isFetched: true,
        data: action.data
      }
    case CREATE_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
        isFetched: false,
        error: true
      }
    default:
      return state
  }
}