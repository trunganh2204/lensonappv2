import React, { Component } from 'react';
import { View, TouchableOpacity, Image, ActivityIndicator, Keyboard, AsyncStorage, Animated } from 'react-native';
import { Text, Toast } from 'native-base';
// Import default color in app
// Import Basic Css 
import logo from '../../../../img/logo.png';
import CryptoJS from 'crypto-js';
import md5 from '../../../libs/md5/md5';
const uuidv1 = require('uuid/v1');
import { connect } from 'react-redux';
import { getCurrentUser } from '../../../redux/actions/get-current-user';
import TextInput from '../../../components/custom-textinput/CustomTextInput';
import { NavigationActions } from 'react-navigation';
import ButtonHotline from '../../../components/button-hotline/ButtonHotline';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
const MARGIN = 50;
const DURATION = 300;
class LoginView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            focusedAnim: new Animated.Value(MARGIN),
        }
        sessionId = '';
        account = '';
        password = '';
        disableOnpress = false;
        isAnimation = true;
    }
    handleFocus(nextInput) {
        this.refs[nextInput].text.focus();
    }
    showToast(text) {
        Toast.show({
            text: text,
            position: 'top',
            buttonText: 'Okay',
            duration: 2000
        })
    }
    setSessionId = async (uuid) => {
        try {
            await AsyncStorage.setItem("key", uuid);
        } catch (error) {

        }
    }
    encryptorPass(userName, password) {
        let value = md5(userName + '.' + password)
        let keyHex = CryptoJS.enc.Utf8.parse(value);
        var encrypted = CryptoJS.DES.encrypt(password, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted
    }
    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove()
    }
    keyboardDidShow(e) {
        if (isAnimation === true) {
            Animated.timing(
                this.state.focusedAnim,
                {
                    toValue: -100,
                    duration: DURATION
                }
            ).start();
        }
    }
    keyboardDidHide(e) {
        if (isAnimation === true) {
            Animated.timing(
                this.state.focusedAnim,
                {
                    toValue: MARGIN,
                    duration: DURATION
                }
            ).start();
        }

    }
    setAnimation(visiable) {
        isAnimation = visiable;
    }
    handerLogin = async (userInfo) => {

        try {
            await AsyncStorage.setItem('account', userInfo.account);
            await AsyncStorage.setItem('fullName', userInfo.fullName);
            await AsyncStorage.setItem('bankName', userInfo.bankName);
            await AsyncStorage.setItem('phoneNum', userInfo.phoneNum);
            await AsyncStorage.setItem('bankNum', userInfo.bankNum)
            this.props.getCurrentUser(userInfo);
            const resetAction = NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Home' })
                ]
            })
            this.props.navigation.dispatch(resetAction);
        } catch (error) {
        }

    }
    login() {
        if (account === '') {
            this.showToast('Tài khoản trống')
            this.setAnimation(true);
            return;
        }
        if (password === '') {
            this.showToast('Mật khẩu trống!');
            this.setAnimation(true);
            return;
        }
        if (disableOnpress === false) {
            this.setAnimation(true);
            disableOnpress = true;
            this.setState({ isLoading: true })
            if (this.props.isOnline === true) {
                sessionId = uuidv1();
                this.setSessionId(sessionId);
                fetch('https://app-dot-casper-electric.appspot.com/api?action=login', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        account: account,
                        password: this.encryptorPass(account, password).toString(),
                        sessionId: sessionId,
                    })
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        if (responseJson.data.status === 0) {
                            if (responseJson.data.loginCode === 0) {
                                this.handerLogin(responseJson.data);
                                disableOnpress = false;
                                isAnimation = true;
                            } else {
                                this.setState({
                                    isLoading: false
                                })
                                switch (responseJson.data.loginCode) {
                                    case -1:
                                        this.showToast('Kiểm tra lại mạng internet!');
                                        break;
                                    case 2:
                                        this.showToast('Tài khoản không tồn tại!');
                                        break;
                                    case 3:
                                        this.showToast(responseJson.data.mess);
                                        break;
                                    default: return null
                                }
                                disableOnpress = false;
                                isAnimation = true;
                            }
                        } else {
                            this.showToast('Tài khoản đã bị khóa!Vui lòng liên hệ hotline');
                            this.setState({
                                isLoading: false
                            })
                            disableOnpress = false;
                            isAnimation = true;
                        }


                    })
                    .catch((error) => {

                    });
            } else {

                this.setState({
                    isLoading: false
                })
                this.showToast('Kiểm tra lại mạng internet!');
                disableOnpress = false;
                isAnimation = true;
            }
        }

    }

    render() {
        const { navigate } = this.props.navigation;
        const marginTop = this.state.focusedAnim;
        return (
            <View style={styles.container}>
                <Animated.View
                    style={{
                        width: '70%',
                        height: 100,
                        alignSelf: 'center',
                        marginTop
                    }}
                >
                    <Image
                        source={logo}
                        style={{ width: '100%', height: '100%' }}
                        resizeMode={'contain'}
                    />
                </Animated.View>


                <View style={styles.preTextInput}>
                    <TextInput
                        label={'Tài khoản'}
                        onSubmitEditing={(e) => {
                            this.setAnimation(false);
                            this.handleFocus(1);
                        }}
                        returnKeyType={'next'}
                        onChangeText={(e) => account = e}
                    />
                    <TextInput
                        label={'Mật khẩu'}
                        onChangeText={(e) => password = e}
                        secureTextEntry
                        onEnter={() => this.setAnimation(true)}
                        ref='1'
                    />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    {this.state.isLoading && <ActivityIndicator />}
                </View>
                <TouchableOpacity
                    onPress={() => {
                        Keyboard.dismiss();
                        this.login();

                    }}
                    style={styles.btnLogin}
                >
                    <Text style={[styles.textStyle, { color: 'white' }]}>ĐĂNG NHẬP</Text>
                </TouchableOpacity>

                <View style={styles.bottomButton}>
                    <TouchableOpacity onPress={() => { navigate('ForgotPassword') }} style={styles.button}>
                        <Text style={styles.textStyle}>Quên mật khẩu ?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { Keyboard.dismiss(); navigate('AccountInfo') }} style={styles.button}>
                        <Text style={[styles.textStyle, { textAlign: 'right' }]}>Đăng ký tài khoản</Text>
                    </TouchableOpacity>
                </View>

                <ButtonHotline
                    style={styles.btnHotline}
                />
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
    },
    btnHotline: {
        marginTop: 50
    },
    btnLogin: {
        width: '90%',
        height: 50,
        alignSelf: 'center',
        borderRadius: 5,
        backgroundColor: '#4493AA',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    textStyle: {
        fontSize: 16,
    },

    textTitle: {
        fontSize: 16,
        marginTop: 10,
        marginBottom: 20,
        color: 'black',
        marginLeft: 5
    },

    textInput: {
        borderWidth: 0.5,
        borderRadius: 5,
        marginBottom: 10,
    },
    textLabel: {
        color: '#bfbfbf',
        fontWeight: 'normal'
    },
    iconColor: '#4493AA',
    preTextInput: {
        width: '100%',
        height: 160,
        paddingLeft: 15,
        paddingRight: 15,
        alignSelf: 'center',
        justifyContent: 'space-around',
    },
    bottomButton: {
        width: '100%',
        height: 50,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    button: {
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        isOnline: state.getOnline.data,
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getCurrentUser: (data) => {
            dispatch(getCurrentUser(data))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);