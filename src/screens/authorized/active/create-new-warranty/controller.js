import * as apiConfig from '../../../../ultis/api/config';
import React, { Component } from 'react';

import { Toast } from 'native-base';
export default class ActivationController {
  _customer = '';
  _customerName = '';
  _serial = '';
  _serail2 = '';
  _phoneNum = '';
  _model = '';
  _activeDate = 0;
  _expriedDate = 0;
  _warrantyCode = '';
  _response = [];
  _reponseProcuts = [];
  _reponseSaveActivation = {};
  _city = 1;
  _responseCreateCustomer = [];
  _body = {
    employeeId: 'nguyentrungkien',
    customerId: 'luongvanhai',
    serial: '2000',
    serial2: '2001',
    warrantyCode: 'xxx'
  }
  // Define method...
  setCustomer(customer) {
    this._customer = customer;
  };
  setPhoneNum(phoneNum) {
    this._phoneNum = phoneNum;
  };
  setResponseSaveActivation(response) {
    this._reponseSaveActivation = response;
  };
  setCustomerName(customerName) {
    this._customerName = customerName;
  };
  setSerial(serial) {
    let upper = serial.toUpperCase()
    this._serial = upper;
  };
  setSerial2(serial2) {
    let upper = serial2.toUpperCase()
    this._serail2 = upper;
  };
  setModel(model) {
    this._model = model;
  };
  setActiveDate(activeDate) {
    this._activeDate = activeDate;
  };
  setExpriedDate(expriedDate) {
    this._expriedDate = expriedDate;
  };
  setWarrantyCode(warrantyCode) {
    this._warrantyCode = warrantyCode;
  };
  setResponse(response) {
    this._response = response;
  };
  setResponseProducts(response) {
    this._reponseProcuts = response;
  };
  getCustomer() {
    return this._customer;
  };
  getPhoneNum() {
    return this._phoneNum;
  }
  getCustomerName() {
    return this._customerName;
  };
  getSerial() {
    return this._serial;
  }
  getSerial2() {
    return this._serail2;
  };
  getModel() {
    return this._model;
  };
  getActiveDate() {
    return this._activeDate;
  };
  getExpriedDate() {
    return this._expriedDate;
  };
  getWarrantyCode() {
    return this._warrantyCode;
  };
  getResponse() {
    return this._response;
  };
  getResponseProducts() {
    return this._reponseProcuts;
  };
  getResponseSaveActivation() {
    return this._reponseSaveActivation;
  };
  getCity() {
    return this._city;
  };
  setCity(city) {
    this._city = city;
  };
  getResponseCreateCustomer() {
    return this._responseCreateCustomer
  };
  setResponseCreateCustomer(response) {
    this._responseCreateCustomer = response;
  };
  gotoHistoryTab(cb) {
    cb.props.navigation.navigate('ActiveHistoryTab')
  }
  cb(data) {
    console.log('Kien max dz', data)
  }


  _showToast(text, type,buttonText, onClose) {
    Toast.show({
      text: text,
      position: 'top',
      buttonText: buttonText,
      type: type,
      onClose: onClose
    })
  }
  checkInvalidPhoneNum(phoneNum) {
   console.log('Fuck',phoneNum)
    if (!(phoneNum.match("[0-9]+"))) return false;
    if (phoneNum.startsWith("09") && phoneNum.length == 10) return true;
    if (phoneNum.startsWith("01") && phoneNum.length == 11) return true;
    if (phoneNum.startsWith("849") && phoneNum.length == 11) return true;
    if (phoneNum.startsWith("841") && phoneNum.length == 12) return true;
    if (phoneNum.startsWith("0") && phoneNum.length == 11) return true;
    return false;
  };
  generalAccountCustomer(string) {
    var temp = "";
    string = '' + string;
    splitstring = string.split(" ");
    for (i = 0; i < splitstring.length; i++)
      temp += splitstring[i];
    return temp;

  } 
  _convertFullName(value) {
    var str;
    str = value;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    //str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");  
    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
    //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-  
    str = str.replace(/^\-+|\-+$/g, "");
    //cắt bỏ ký tự - ở đầu và cuối chuỗi 
    let result = this.generalAccountCustomer(str) + this.getPhoneNum()
    this.setCustomer(result)
    console.log('ADFAS', result)
    return result;
  }
  async sendRequestSaveActivation(activation, cb) {
    activation.setActiveDate(new Date().getTime())
    console.log('employee', this.getSerial(), this.getSerial2()) 

    cb._gotoModalLoading(true)
    await fetch((`https://casper-electric.appspot.com/api?action=saveActivation`), {

      method: 'POST',
      body: JSON.stringify({
        employeeId: cb.props.currentUser,
        customerId: this.getCustomer(),
        serial: this.getSerial(),
        serial2: this.getSerial2(),
        guaranteeCode: this.getWarrantyCode(),

      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then(async (responJSON) => {
        console.log('ket qua tra ve', responJSON)
        await this.setResponseSaveActivation(responJSON.data);
        //  console.log('CCCCCCC', this.getResponseSaveActivation().activationStatus)
        if (this.getResponseSaveActivation().activationStatus === 5) {
          cb.setState({
            isLoading: false,
            customerLoading: false,
            CustomerName: '',
            phoneNum: '',
            serialText: '',
            customerText: '',
            serial2Text: '',
            fromDateText: '',
            toDateText: '',
            activeCode: '',
            searchLoading: false,
            showSearchPanel: -1,
            resultCheck: 1,
            errorType: '',
            labelCustomerName: 'Tên khách hàng...',
            labelPhoneNum: 'Số điện thoại...',
            labelSerial: 'Serial cục nóng...',
            labelSerial2: 'Serial cục lạnh...',
            labelActiveCode: 'Mã kích hoạt bảo hành...',
            labelCustomerNameStyle: { color: 'black' },
            labelPhoneNumStyle: { color: 'black' },
            labelSerialStyle: { color: 'black' },
            labelSerial2Style: { color: 'black' },
            labelActiveCodeStyle: { color: 'black' },
            labelCustomerNameStyle: { color: 'black' },
            labelPhoneNumStyle: { color: 'black' },
            labelSerialStyle: { color: 'black' },
            labelSerial2Style: { color: 'black' },
            labelActiveCodeStyle: { color: 'black' },
            titleReturn: ' Kích hoạt thành công!!!'
          })
          this._showToast('Kích hoạt thành công','success','Xem',() => cb.props.navigation.navigate('ActiveHistoryTab'))
        }
        else {
          cb._checkActivationStatus(this.getResponseSaveActivation().activationStatus)
        }

        cb._gotoModalLoading(false)
      })
      .catch((error) => { console.log(error) })
      ;
  };
  searchCustomer(text, cb) {
    cb.setState({ customerLoading: true })
    if (text === '') {
      cb.setState({
        customerLoading: false,
        showSearchPanel: -1,
        noteCustomer: ''
      })
    }
    else {
      fetch(`https://app-dot-casper-electric.appspot.com/api?action=search&keyword=${text}&typeIndexName=UserIndex`)
        .then((response) => response.json())
        .then((responJSON) => {
          this.setResponse(responJSON.data);
          console.log('ABCDEF', this.getResponse())
          cb.setState({
            showSearchPanel: 0,
            customerLoading: false,
            mess: 'Khách hàng chưa tồn tại, có thể tạo mới'
          })
        })
        .catch((error) => { console.log(error) })
        ;
    }


  };
  async searchProducts(text, cb) {
    cb.setState({ customerLoading: true, serialText: text })
    // them type
    await fetch(`https://app-dot-casper-electric.appspot.com/api?action=getProductBySerial&serial=${text}`)
      .then((response) => response.json())
      .then(async (responJSON) => {
        await this.setResponseProducts(responJSON.data);
        console.log(this.getResponseProducts())
        cb.setState({
          serial2Text: this.getResponseProducts().serial2,
          customerLoading: false,

        })

      })
      .catch((error) => { console.log(error) })
      ;

  };
  saveActivation(customer, cb) {
    fetch(apiConfig.convertURL(apiConfig.actionOfAPI.SAVE_ACTIVATION), {

      method: 'POST',
      body: JSON.stringify({
        customer: this.getCustomer(),
        serial: this.getSerial(),
        serial2: this.getSerial2(),
        activeDate: this.getActiveDate(),
        expriedDate: this.getExpriedDate(),
        warrantyCode: this.getWarrantyCode()

      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then((responJSON) => {
        this.setResponse(responJSON.data);
        cb.setState({ isLoading: false })
      })
      .catch((error) => { console.log(error) })
      ;
  }
  async   sendRequestCreateCustomer(cb) {
    cb.setState({ isLoading: true })
    await fetch('https://app-dot-casper-electric.appspot.com/api?action=signUp', {

      method: 'POST',
      body: JSON.stringify({

        fullName: this.getCustomerName(),
        account: this.getCustomer(),
        email: 'khachhang@gmail.com',
        phoneNum: this.getPhoneNum(),
        addressCity: this.getCity(),
        city: this.getCity()

      }),
      headers: { "Content-Type": "application/json; charset=utf-8" }
    })
      .then((response) => response.json())
      .then((responJSON) => {

        console.log('ket qua setResonpse', responJSON.data)
        if (responJSON.data.loginCode == 1) {
          cb.setState({  isLoading: false, mess: 'Tài khoản đã đăng kí , chọn tài khoản khác' });
          this._showToast(cb.state.mess,'warning')
          return
        }
        else {
          this.setResponseCreateCustomer(responJSON.data);
          cb.setState({
            isLoading: false,
            mess: ''
          })
          //this._showToast('Tạo mới thành công, điền tiếp thông tin máy...','warning','OK',cb._handlerNextInput(3))
          return
        }

      })
      .catch((error) => { console.log(error) })
      ;

  };
}