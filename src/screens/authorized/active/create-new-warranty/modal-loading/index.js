import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, Modal, Image, ActivityIndicator } from 'react-native';
import { Text } from 'native-base';
import BasicView from '../../../../../ultis/styles/AppStyles';
import color from '../../../../../ultis/styles/common-css';
import deleteIcon from '../../../../../../img/delete.png';
class ModalLoading extends Component {
     constructor(props) {
          super(props)
          this.state = {
               isShow:false,
               dataModel: {}
          }

     }
     
     _setShow(visible) {
          this.setState({
               isShow: visible,
          });
     }
     
     render() {
          
          return (
               <Modal
                    style={styles.container}
                    animationType={"slide"}
                    visible={this.state.isShow}
                    transparent={true}
                    onRequestClose={() => this._setShow(false)}
               >
                    <View style={styles.outsideContainer} />
                    <View style={styles.content}>
                         <ActivityIndicator />
                    </View>
               </Modal>
          );
     }
}
const screen = Dimensions.get('window');
const numberRadius = 5;
const styles = {
     container: {
          width: screen.width,
          height: screen.height,
          justifyContent: 'center',
          alignItems: 'center'
     },
     titleItem: {
          flex: 0.35,
          fontSize: 12,
          marginLeft: 20
     },
     valueItem: {
          flex: 0.65,
          fontSize: 12,
          marginLeft: 20
     },
     outsideContainer: {
          flex: 1,
          backgroundColor: '#000000',
          width: screen.width,
          opacity: 0.6,
          position: 'absolute',
          height: screen.height
     },
     header: {
          height: 50,
          width: '100%',
          backgroundColor: color.colorBlue,
          justifyContent: 'center',
          alignItems: 'center',
          borderTopLeftRadius: numberRadius,
          borderTopRightRadius: numberRadius,
          

     },
     textTitle: {
          color: color.colorWhite,
          fontSize: 15,
     },

     content: {
          width: 30,
          height: 30,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'transparent',
          borderRadius: numberRadius,
          alignSelf: 'center',
          marginTop: screen.height / 2


     },
     onPressDelete: {
          position: 'absolute',
          right: 0,
          width: 50,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center'
     },
     iconDelete: {
          width: 15,
          height: 15,
     },

}
export default ModalLoading;
/**
 *  
 */