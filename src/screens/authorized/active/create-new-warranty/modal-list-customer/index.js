import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, Modal, Image,FlatList } from 'react-native';
import { Text } from 'native-base';
import BasicView from '../../../../../ultis/styles/AppStyles';
import color from '../../../../../ultis/styles/common-css';
import deleteIcon from '../../../../../../img/delete.png';
class ModalListCustomer extends Component {
     constructor(props) {
          super(props)
          this.state = {
               isShow: false,
               dataModel: {}
          }

     }

     _setShow(visible) {
          this.setState({
               isShow: visible,
          });
     }
     _renderItem = ({item, index}) => (
          <TouchableOpacity
               onPress={() => {
                    this.props.getDataBack(item);
                    this._setShow(false)
               }} 
               style={styles.itemContainer}
          >
               <Text>{index + 1}</Text>
               <Text style={styles.titleItem}>{item.fullName}</Text>
               <Text style={styles.valueItem}>{item.phoneNum}</Text>
          </TouchableOpacity>
     )
     render() {
          return (
               <Modal
                    style={styles.container}
                    animationType={"slide"}
                    visible={this.state.isShow}
                    transparent={true}
                    onRequestClose={() => this._setShow(false)}
               >
                    <View style={styles.outsideContainer} />
                    <View style={styles.content}>
                         <View style={styles.header}>
                              <Text style={styles.textTitle}>Danh sách khách hàng</Text>
                              <TouchableOpacity
                                   style={styles.onPressDelete}
                                   onPress={() => this._setShow(false)}
                              >
                                   <Image
                                        source={deleteIcon}
                                        style={styles.iconDelete}
                                        resizeMode={'center'}
                                   />
                              </TouchableOpacity>

                         </View>
                         <FlatList 
                              data={this.props.listCustomer}
                              extraData={this.state}
                              keyExtractor={item => item.id}
                              renderItem={this._renderItem}
                         />
                    </View>
               </Modal>
          );
     }
}
const screen = Dimensions.get('window');
const numberRadius = 5;
const styles = {
     container: {
          width: screen.width,
          height: screen.height,

     },
     titleItem: {
          flex: 0.45,
          
          marginLeft: 20
     },
     valueItem: {
          flex: 0.55,
          
          marginLeft: 20
     },
     outsideContainer: {
          flex: 1,
          backgroundColor: '#000000',
          width: screen.width,
          opacity: 0.6,
          position: 'absolute',
          height: screen.height
     },
     header: {
          height: 50,
          width: '100%',
          backgroundColor: color.colorBlue,
          justifyContent: 'center',
          alignItems: 'center',
          borderTopLeftRadius: numberRadius,
          borderTopRightRadius: numberRadius,


     },
     textTitle: {
          color: color.colorWhite,
          fontSize: 15,
     },

     content: {
          width: '100%',
          height: '100%',
          backgroundColor: 'white',
          borderRadius: numberRadius,
          alignSelf: 'center',
          marginTop: 20,
     },
     onPressDelete: {
          position: 'absolute',
          right: 0,
          width: 50,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center'
     },
     iconDelete: {
          width: 15,
          height: 15,
     },
     itemContainer: {
          width: '100%',
          height: 50,
          borderWidth: 1,
          backgroundColor: '#fff',
          borderRadius: 2,
          borderColor: '#ddd',
          borderBottomWidth: 0,
          shadowColor: '#000',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.8,
          shadowRadius: 2,
          elevation: 1,
          flexDirection: 'row',
          alignItems: 'center'
     }
}
export default ModalListCustomer;
/**
 *  
 */