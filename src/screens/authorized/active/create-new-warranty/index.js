import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Text } from 'native-base';
import { NavigationActions } from 'react-navigation'
//Redux
import { connect } from 'react-redux';
import Header from '../../../../components/header/Header';
import * as ActionCreator from '../../../../redux/actions';
import * as apiConfig from '../../../../ultis/api/config';
import color from '../../../../ultis/styles/common-css';
import CustomTextInput from '../../../../components/text-input/text-input';
import ModalCreateUser from '../../../../components/modal-create-user/ModalCreateUser';
import ModalLoading from './modal-loading';
import BasicView from '../../../../ultis/styles/AppStyles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DatePicker from '../../../../components/date-picker';
import Controller from './controller';
import ModalBarCode from './modal-barcode';
import ModalListCustomer from './modal-list-customer';
import CustomPicker from '../../../../components/custom-picker';
import dataProvince from '../../../../ultis/api/local-data/province.json';
import ButtonHotline from '../../../../components/button-hotline/ButtonHotline';

class NewWarranty extends Component {
    static navigationOptions = {
        tabBarLabel: 'Kích hoạt'
    }
    constructor(props) {
        super(props);
        this.goToCreateUser = this.goToCreateUser.bind(this);
        controller = new Controller();
        this.state = {
            // một text input có 2 biến sate gồm giá trị value và nhãn label để hiển thị ban đầu và hiện  thị lại khi gặp lỗi...
            isLoading: false,
            customerLoading: false,
            CustomerName: '',
            phoneNum: '',
            serialText: '',
            customerText: '',
            serial2Text: '',
            fromDateText: '',
            toDateText: '',
            activeCode: '',
            searchLoading: false,
            showSearchPanel: -1,
            resultCheck: 1,
            errorType: '',
            labelCustomerName: 'Tên khách hàng...',
            noteCustomer: 'Ví dụ: Nguyễn Hữu Long',
            labelPhoneNum: 'Số điện thoại khách hàng...',
            notePhoneNum: '',
            labelSerial: 'Serial cục nóng...',
            noteSerial: 'Ví dụ: 1234567890',
            labelSerial2: 'Serial cục lạnh...',
            noteSerial2: 'Ví dụ: 0987654321',
            labelActiveCode: 'Mã kích hoạt...',
            noteActiveCode: 'Xem trên thẻ cào',
            labelCustomerNameStyle: { color: 'black' },
            labelPhoneNumStyle: { color: 'black' },
            labelSerialStyle: { color: 'black' },
            labelSerial2Style: { color: 'black' },
            labelActiveCodeStyle: { color: 'black' },
            labelCustomerNameStyle: { color: 'black' },
            labelPhoneNumStyle: { color: 'black' },
            labelSerialStyle: { color: 'black' },
            labelSerial2Style: { color: 'black' },
            labelActiveCodeStyle: { color: 'black' },
            titleReturn: '',
            typeCasette: false,
            typeSerial: 1,
            lableAdress: 'Địa chỉ khách hàng...',
            selectedCustomer: false,
            renderButtonCreate: false,
            mess: '',

        }
        key = 0
    }
    //Tự động chuyển xuống textinput tiếp theo khi nhấn enter
    componentWillReceiveProps() {
        if (this.props.isOnline) {
            this._showToast('Không có internet', 'danger', 'OK');
            return
        }
    }
    _handlerNextInput(inputRefs) {
        this.refs[inputRefs].text.focus();
    }
    // Xoá khoảng trắng khi nhập liệu giá trị ...
    _ignoreSpaces = (string) => {
        var temp = "";
        string = '' + string;
        splitstring = string.split(" ");
        for (i = 0; i < splitstring.length; i++)
            temp += splitstring[i];
        return temp;
    }
    // Tìm kiếm khách hàng tồn tại trên hệ thống...
    _searchCustomer(fullName) {
        let CustomerName = this._ignoreSpaces(fullName);
        // console.log('Kien max dep trai', `${apiConfig.convertURL(apiConfig.actionOfAPI.SEARCH, CustomerName, 'UserIndex')}`)
        this.props.searchCustomer(apiConfig.convertURL(apiConfig.actionOfAPI.SEARCH, CustomerName, 'UserIndex'))
        this.setState({
            renderDefault: false
        })

    }
    _renderSearchPanel(value) {
        switch (value) {
            case -1: return null;
            case 0: return (this._renderSearchDialog())
        }
    }
    // Show Create new customer panel...
    goToCreateUser() {
        this.ModalCreateUser._setShow(true)
    }
    // Show Progress and disable all button...
    _gotoModalLoading(visible) {
        this.ModalLoading._setShow(visible)
    }
    // Show scan barcode panel...
    _toggleModalBarcode(visible) {
        this.ModalBarCode.setShowBarCode(visible)
    }
    // Show list customer if list.length > 5
    _toggleListCustomer(visible) {
        this.ModalListCustomer._setShow(visible)
    }
    _renderSearchDialog() {
        if (controller.getResponse().length == 0) {
            return <Text style={{ color: 'red' }}>{this.state.mess}</Text>
        }
        else {
            return (
                <View>
                    {
                        controller.getResponse().map((e, i) => {
                            if (i < 5) {
                                return (
                                    <View key={i} style={{ marginBottom: 10 }}>
                                        <Text onPress={() => {
                                            controller.setCustomer(e.id);
                                            controller.setPhoneNum(e.phoneNum)
                                            this.setState({
                                                selectedCustomer: true,
                                                CustomerName: e.fullName,
                                                phoneNum: e.phoneNum,
                                                showSearchPanel: -1,
                                                resultCheck: 1,
                                                labelCustomerName: 'Nhập thành công...',
                                                labelCustomerNameStyle: { color: 'black' }
                                            });
                                            //this._handlerNextInput(3)
                                        }}
                                        >
                                            {i + 1}. {e.fullName}-{e.phoneNum}...
                                        </Text>
                                    </View>
                                )
                            }
                        })
                    }
                    {
                        (controller.getResponse().length > 5) ? (
                            <Text
                                style={{ alignSelf: 'center', color: 'red', marginBottom: 10 }}
                                onPress={() => this._toggleListCustomer(true)}
                            >...Xem tất cả...</Text>
                        ) : null
                    }
                </View>
            )

        }
    }
    // Kiem tra nguoi dùng có nhập trống trường nào không? nếu có thì không cho active
    _checkInput(cb) {
        const {
            CustomerName, serialText, serial2Text,
            activeCode, fromDateText, toDateText,
            labelCustomerName, labelPhoneNum, labelSerial,
            labelSerial2, labelActiveCode, phoneNum, selectedCustomer, renderButtonCreate
            } = cb.state
        if (!this.props.isOnline) {
            controller._showToast('Mất kết nối, kiểm tra lại internet...', 'danger', 'OK');
            return false
        }
        if (this.props.currentUser === '') {
            controller._showToast('Lỗi kết nối, vui lòng thử lại sau', 'warning', 'OK');
            return false

        }
        if (selectedCustomer === false && renderButtonCreate === false) {
            controller._showToast('Chưa tạo mới khách hàng', 'danger', 'Sửa')
            return false
        }
        if (CustomerName === '') {
            cb.setState({ resultCheck: 0, labelCustomerName: 'Sai thông tin khách hàng...', labelCustomerNameStyle: { color: 'red' } })
            return false;
        }
        if (phoneNum === '') {
            cb.setState({ labelPhoneNum: 'Số điện thoại trống...', labelPhoneNumStyle: { color: 'red' } })
            return false;
        }
        if (serialText === '') {
            cb.setState({ labelSerial: 'Kiểm tra serial cục nóng...', labelSerialStyle: { color: 'red' } })
            return false;
        }
        if (serial2Text === '') {
            cb.setState({ labelSerial2: 'Kiểm tra serial cục lạnh...', labelSerial2Style: { color: 'red' } })
            return false;
        }
        if (activeCode === '') {
            cb.setState({ labelActiveCode: 'Mã kích hoạt trống...', labelActiveCodeStyle: { color: 'red' } })
            return false;
        }
        // if (controller.getResponseCreateCustomer().length === 0) {
        //     cb.setState({ titleReturn: '', errorType: 'Sai thông tin khách hàng..' })
        //     return false;
        // }
        if (cb.props.currentUser === '' || cb.props.currentUser === undefined) {
            cb.setState({ errorType: 'Kiểm tra lại kết nối mạng ' })
            return false;
        }
        else {
            controller.sendRequestSaveActivation(controller, this);
            return true
        }
    }
    _renderErrorLog(value) {
        switch (value) {
            case 0: return (
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'red' }}>Điền thiếu thông tin, vui lòng hoàn thành...</Text>
                </View>
            )
            default: return null
        }
    }
    // Kiem tra ket qua kich hoat
    _checkActivationStatus(value) {
        switch (value) {
            case 1: this.setState({ labelSerial: 'Sai serial cục nóng...', labelSerialStyle: { color: 'red' }, labelSerial2: 'Serial cục lạnh...', labelSerial2Style: { color: 'black' }, errorType: 'Kích hoạt không thành công', titleReturn: '', labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break
            case 2: this.setState({ labelSerial2: ' Sai serial cục lạnh...', labelSerial2Style: { color: 'red' }, labelSerial: 'Serial cục nóng...', labelSerialStyle: { color: 'black' }, errorType: 'Kích hoạt không thành công', titleReturn: '', labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break
            case 3: this.setState({ errorType: '2 serial không cùng 1 model...', labelSerial: 'Kiểm tra lại serial...', labelSerial2: 'Kiểm tra lại serial...', labelSerialStyle: { color: 'red' }, labelSerial2Style: { color: 'red' }, titleReturn: '', labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break
            case 6: this.setState({ errorType: 'Model đã được kích hoạt rồi...', labelSerial: 'Kiểm tra lại serial...', labelSerial2: 'Kiểm tra lại serial...', labelSerialStyle: { color: 'red' }, labelSerial2Style: { color: 'red' }, titleReturn: '', labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break
            case 7: this.setState({ labelSerial2: ' Serial không được trùng nhau...', labelSerial2Style: { color: 'red' }, labelSerial: 'Serial không được trùng nhau ', labelSerialStyle: { color: 'red' }, errorType: 'Kích hoạt không thành công', titleReturn: '', labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break;
            case 8: this.setState({ errorType: 'Kích hoạt không thành công', labelSerial: 'Serial đã được kích hoạt với model khác...', labelSerialStyle: { color: 'red' }, labelSerial2: 'Serial cục lạnh...', labelSerial2Style: { color: 'black' }, titleReturn: '', labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break
            case 9: this.setState({ errorType: 'Kích hoạt không thành công', labelSerial2: 'Serial đã được kích hoạt với model khác...', labelSerial2Style: { color: 'red' }, labelSerial: 'Serial cục nóng...', labelSerialStyle: { color: 'black' }, titleReturn: '', labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break
            case 10: this.setState({ labelSerial: 'Sai serial cục nóng...', labelSerialStyle: { color: 'red' }, labelSerial2: ' Sai serial cục lạnh...', labelSerial2Style: { color: 'red' }, errorType: 'Kích hoạt không thành công', titleReturn: '' }); break
            case 11: this.setState({ errorType: 'Kích hoạt không thành công', labelSerial: 'Serial đã được kích hoạt với model khác...', labelSerialStyle: { color: 'red' }, labelSerial2: 'Serial đã được kích hoạt với model khác...', labelSerial2Style: { color: 'red' }, labelActiveCode: 'Mã kích hoạt', labelActiveCodeStyle: { color: 'black' } }); break
            case 12: this.setState({errorType: 'Kích hoạt không thành công', labelActiveCode: 'Mã kích hoạt đã được sử dụng', labelActiveCodeStyle: {color: 'red'}}); break
            default: this.setState({ errorType: 'Kích hoạt không thành công', labelActiveCode: 'Mã kích hoạt không đúng', labelActiveCodeStyle: {color: 'red'} }); break
        }
    }
    render() {
        const backAction = NavigationActions.back();
        aa = controller.getCustomer();
        bb = controller.getPhoneNum()
        //console.log('ABC', aa, 'CCC', bb)
        //console.log('ADF',controller.getCity() )

        return (
            <KeyboardAwareScrollView
                style={styles.container}
                resetScrollToCoords={{ x: 0, y: 0 }}
                scrollEnabled={true}
            >
                <Header
                    title={'Kích hoạt sản phẩm'}
                    navigation={this.props.navigation}
                />


                <View style={styles.childContainer}>
                    {/** Text input so dien thoai khách hàng */}
                    <CustomTextInput
                        value={this.state.phoneNum}
                        label={this.state.labelPhoneNum}
                        labelStyle={this.state.labelPhoneNumStyle}
                        //imageIcon={true}
                        ref="7"
                        labelStyle={this.state.labelStyle}
                        keyboardType={'numeric'}
                        returnKeyType={'next'}
                        onSubmitEditing={(text) => {
                            if (controller.checkInvalidPhoneNum(text) === true) {
                                controller.searchCustomer(text, this);
                            }
                            else {
                                alert('Fuck')
                            }


                        }}
                        onChange={(text) => {
                            this.setState({ phoneNum: text })
                        }}
                        customOnBlur={async (text) => {
                            //console.log(this.state.phoneNum)
                            let checkPhoneNum = await controller.checkInvalidPhoneNum(text);
                            if (checkPhoneNum) {
                                controller.setPhoneNum(text);
                                let aaa = controller.getPhoneNum();
                                controller.searchCustomer(text, this);
                                return
                            }
                            else {
                                controller._showToast('Điền sai số điện thoại', 'warning', 'Sửa', () => { this.setState({ phoneNum: '' }); this._handlerNextInput(7) });
                                return
                            }
                        }}
                        onEndEditting={(e) => { console.log(e.nativeEvent.text) }}
                        onFocus={() => {
                            if (this.state.selectedCustomer) {
                                this.setState({
                                    selectedCustomer: false,
                                    phoneNum: '',
                                    CustomerName: '',
                                    labelCustomerName: 'Tên khách hàng...',
                                });
                                this._handlerNextInput(7)
                                return
                            }
                            else {
                                this._handlerNextInput(7)

                                return
                            }
                        }}
                    />
                    <View>
                        {
                            (this.state.customerLoading || this.state.isLoading) && <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator />
                                <Text>Đang tải, vui lòng chờ...</Text>
                            </View>
                        }
                        {
                            this._renderErrorLog(this.state.resultCheck)
                        }
                        {
                            this._renderSearchPanel(this.state.showSearchPanel)
                        }

                    </View>
                    {/** Text input tên khách hàng */}
                    <CustomTextInput
                        value={this.state.CustomerName}
                        label={this.state.labelCustomerName}
                        labelStyle={this.state.labelCustomerNameStyle}
                        //imageIcon={true}
                        note={this.state.noteCustomer}
                        //source={require('../../../../../img/add.png')}
                        ref="8"
                        onPressIcon={this.goToCreateUser}
                        onSubmitEditing={(text) => {
                            controller.searchCustomer(text, this);
                            //this._searchCustomer(text) ;  <== this command dispatch action using redux
                        }}
                        onChange={(text) => {
                            this.setState({ CustomerName: text })
                        }}
                        customOnBlur={async (text) => {
                            controller.setCustomerName(text)
                            await controller.searchCustomer(text, this);
                            controller._convertFullName(text);
                            if (!this.state.selectedCustomer) {
                                this.setState({ renderButtonCreate: true })
                            }
                        }}

                        onFocus={() => {
                            if (this.state.selectedCustomer) {
                                this.setState({
                                    selectedCustomer: false,
                                    phoneNum: '',
                                    CustomerName: '',
                                    labelCustomerName: 'Tên khách hàng...',
                                });
                                this._handlerNextInput(8)
                                return
                            }
                            else {
                                this._handlerNextInput(8)

                            }
                        }}
                    />
                    {/**
                     * Nếu là tạo mới khách hàng thì mới render địa chỉ để chọn...
                     */}
                    {
                        !this.state.selectedCustomer ? <CustomTextInput

                            label={this.state.lableAdress}
                            labelStyle={this.state.labelCustomerNameStyle}
                            //imageIcon={true}
                            note={this.state.noteCustomer}
                            source={require('../../../../../img/add.png')}
                            onPressIcon={this.goToCreateUser}
                            onSubmitEditing={(text) => {
                                controller.searchCustomer(text, this);
                                //this._searchCustomer(text) ;  <== this command dispatch action using redux
                            }}
                            onChange={(text) => {
                                this.setState({ CustomerName: text })
                            }}
                            editable={false}
                            picker

                            checkPickerSelected={(value) => {
                                if (value == 1) {
                                    this.setState({ renderButtonCreate: true })
                                }
                                else {
                                    this.setState({ renderButtonCreate: true }),
                                        controller.setCity(value)
                                }
                            }
                            }

                        /> : null
                    }
                    {
                        ((this.state.selectedCustomer === false && this.state.renderButtonCreate === false) || (this.state.selectedCustomer)) ? (
                            <View>
                                <CustomTextInput
                                    value={this.state.serialText}
                                    label={this.state.labelSerial}
                                    labelStyle={this.state.labelSerialStyle}
                                    note={this.state.noteSerial}
                                    ref="3"
                                    imageIcon={true}
                                    onPressIcon={() => { this._toggleModalBarcode(true); key = 1 }}
                                    source={require('../../../../../img/barcode.png')}
                                    returnKeyType={'next'}
                                    onSubmitEditing={(text) => {
                                        // controller.searchProducts(text, this);
                                        controller.setSerial(text);
                                        this.setState({ serialText: text })
                                        this._handlerNextInput(4)
                                    }}
                                    onChange={(text) => {
                                        controller.setSerial(text);
                                        this.setState({ serialText: text })
                                    }}
                                />
                                {/** Text input cuc lanh */}
                                <CustomTextInput
                                    value={this.state.serial2Text}
                                    label={this.state.labelSerial2}
                                    ref="4"
                                    note={this.state.noteSerial2}
                                    labelStyle={this.state.labelSerial2Style}
                                    imageIcon={true}
                                    onPressIcon={() => { this._toggleModalBarcode(true); key = 2 }}
                                    source={require('../../../../../img/barcode.png')}
                                    returnKeyType={'next'}
                                    onSubmitEditing={(text) => {
                                        controller.setSerial2(text);
                                        this.setState({ serial2Text: text });
                                        this._handlerNextInput(5)
                                    }}
                                    onChange={(text) => { controller.setSerial2(text); this.setState({ serial2Text: text }) }}
                                />

                                {/** Text input ma kich hoat */}
                                <CustomTextInput
                                    value={this.state.activeCode}
                                    label={this.state.labelActiveCode}
                                    labelStyle={this.state.labelActiveCodeStyle}
                                    ref="5"
                                    note={this.state.noteActiveCode}
                                    returnKeyType={'next'}
                                    onSubmitEditing={(e) => { }}
                                    onChange={(text) => { controller.setWarrantyCode(text); this.setState({ activeCode: text }) }}
                                />
                                <Text style={{ color: 'red' }}>{this.state.errorType}</Text>
                                <Text style={{ color: color.colorBlue }}>{this.state.titleReturn}</Text>
                                <TouchableOpacity
                                    style={[styles.buttonContainer, { backgroundColor: color.colorBlue, width: screen.width / 2 }]}
                                    onPress={async () => {
                                        await this._checkInput(this);

                                        //console.log(controller.getResponseSaveActivation())
                                    }}
                                >
                                    <Text style={styles.textTitle}>Kích hoạt</Text>
                                </TouchableOpacity>
                            </View>
                        ) : <TouchableOpacity
                            style={{ height: 50, backgroundColor: color.colorBlue, justifyContent: 'center', alignItems: 'center', borderRadius: 5, width: screen.width / 2, marginTop: 20 }}
                            onPress={async () => {
                                await controller.sendRequestCreateCustomer(this)
                                this.setState({ selectedCustomer: true, renderButtonCreate: false });
                            }}
                        >
                                <Text style={styles.textTitle}>Tạo mới khách hàng</Text>
                            </TouchableOpacity>
                    }
                    {/** Text input cuc nong */}



                </View>
                <ButtonHotline style={{ marginBottom: 10, marginTop: 10 }} />
                <ModalCreateUser
                    ref={(ref) => this.ModalCreateUser = ref}
                    aaa={(e) => {
                        controller.setCustomer(e.id);
                        controller.setResponse(e);
                        this.setState({
                            CustomerName: e.fullName,
                            phoneNum: e.phoneNum,
                            showSearchPanel: -1
                        })
                    }}
                />
                <ModalLoading
                    ref={(ref) => this.ModalLoading = ref}
                />
                <ModalBarCode
                    ref={(ref) => this.ModalBarCode = ref}
                    getCodeIMEI={(codeIMEI) => {
                        if (key === 1) {
                            this.setState({ serialText: codeIMEI });
                            controller.setSerial(codeIMEI)
                            this._handlerNextInput(4)
                        }
                        if (key === 2) {
                            this.setState({ serial2Text: codeIMEI });
                            controller.setSerial2(codeIMEI)
                            this._handlerNextInput(5)
                        }
                    }}
                />
                <ModalListCustomer
                    ref={(ref) => this.ModalListCustomer = ref}
                    listCustomer={controller.getResponse()}
                    getDataBack={(e) => {
                        controller.setCustomer(e.id);
                        this.setState({
                            CustomerName: e.fullName,
                            phoneNum: e.phoneNum,
                            showSearchPanel: -1
                        })
                    }}
                />

            </KeyboardAwareScrollView>
        );
    }
}


const screen = Dimensions.get('window')
const styles = {
    datepickerStyles: {
        borderWidth: 5,
        borderColor: color.colorBlue,
        justifyContent: 'space-between',

    },
    container: {
        //backgroundColor: '#fff',
        backgroundColor: '#F1F1F1',
        flex: 1,
    },
    childContainer: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 10
    },
    header: {
        height: 50,
        width: screen.width,
        backgroundColor: color.backgroundColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textTitle: {
        color: color.colorWhite,
        fontSize: 15,
        fontWeight: 'bold'
    },
    buttonContainer: {
        height: 40,
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: 10,
        alignSelf: 'center',
        marginTop: 10

    },
    addButton: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        width: screen.width - 20,
        backgroundColor: color.colorBlue,
        borderRadius: 5
    },
    customPicker: {
        borderBottomWidth: 0,
        borderColor: color.colorBlue,
        width: screen.width / 2,
        height: 50,
        position: 'absolute',
        top: 0,
        right: 0,
        backgroundColor: 'red'
    },
    parentView: {
        backgroundColor: 'green'
    }
}
const mapStateToProps = state => {
    return {
        currentUser: state.getCurrentUser.data,
        isOnline: state.getOnline.data,
    }
}

export default connect(mapStateToProps)(NewWarranty);
/**
 *   <TouchableOpacity onPress={()=>{controller.handleRequest()}} style={{ alignSelf: 'center'}}>
                    <Text>AAAA</Text>
                </TouchableOpacity>
 */
/**
 *  <View>
                        {
                            this.props.isFetching && <ActivityIndicator />
                        }
                        {
                            (this.props.customer.length == 0) ? <Text style={{ color: 'red' }}>Khách hàng không tồn tại , nhập lại hoặc tạo mới...{this.props.customer.length}</Text>
                                : (
                                    <View>
                                        {this.props.customer.map((e, i) => {
                                            return (
                                                <TouchableOpacity key={i} onPress={() => { this.setState({ CustomerName: e.fullName +'-'+ e.phoneNum +'-'+ e.email }) }}>
                                                    <Text>{e.fullName}</Text>
                                                </TouchableOpacity>
                                            )
                                        })}
                                    </View>
                                )
                        }
                    </View>
                     <CustomTextInput
                        label={'Ngày kích hoạt ...'}
                        returnKeyType={'next'}
                        onSubmitEditing={() => { }}
                        onChange={(text) => {controller.setActiveDate(text)}}
                    />
                    <CustomTextInput
                        label={'Ngày hết hạn bảo hành ...'}
                        returnKeyType={'next'}
                        onSubmitEditing={() => { }}
                        onChange={(text) => {controller.setExpriedDate(text)}}
                    />
 */
/**
 * 
 * 
 *  <View style={[BasicView.horizontalView, { width: screen.width, justifyContent: 'space-around', marginTop: 20 }]}>
                        <DatePicker
                            getValue={(value) => { controller.setActiveDate(new Date(value).getTime()); this.setState({ fromDateText: value }) }}
                            showIcon={false}
                            style={styles.datepickerStyles}
                            title={'Ngày kích hoạt'}
                        />
                        <DatePicker
                            getValue={(value) => { controller.setExpriedDate(new Date(value).getTime()); this.setState({ toDateText: value }) }}
                            showIcon={false}
                            style={styles.datepickerStyles}
                            title={'Ngày hết hạn'}
                        />
                    </View>
 */



