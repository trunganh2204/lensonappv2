import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, Modal, Image } from 'react-native';
import { Text } from 'native-base';
import BasicView from '../../../../../ultis/styles/AppStyles';
import color from '../../../../../ultis/styles/common-css';
import deleteIcon from '../../../../../../img/delete.png';
class ModalDetailActivation extends Component {
     constructor(props) {
          super(props)
          this.state = {
               isShow: false,
               dataModel: {}
          }

     }
     _renderDateMonthYear(value) {
          let date = new Date(value).getDate();
          let month = new Date(value).getMonth() + 1;
          let year = new Date(value).getFullYear();
          return `${date}/${month}/${year}`
     }
     _setShow(visible) {
          this.setState({
               isShow: visible,
          });
     }
     _renderContent() {
          if (this.state.dataModel == undefined) {
               return null
          }
          else {
               if (this.state.dataModel.custommer == undefined) {
                    return null
               }
               else {
                    return (
                         <View style={{ flex: 1, justifyContent: 'space-between', marginBottom: 20, marginTop: 20}}>
                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={[styles.titleItem, { fontWeight: 'bold', fontSize: 12 }]}>Khách hàng</Text>
                                   <Text style={{ fontWeight: 'bold', fontSize: 12,flex:0.65 , marginLeft: 20}}>{this.state.dataModel.custommer.fullName}</Text>
                              </View>
                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={[styles.titleItem, { fontWeight: 'bold', fontSize: 12 }]}>Số điện thoại </Text>
                                   <Text style={{ fontWeight: 'bold', fontSize: 12,flex:0.65 , marginLeft: 20}}>{this.state.dataModel.custommer.phoneNum}</Text>
                              </View>
                            

                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={styles.titleItem}>Email</Text>
                                   <Text style={styles.valueItem}>{this.state.dataModel.custommer.email}</Text>
                              </View>
                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={styles.titleItem}>Tên sản phẩm</Text>
                                   <Text style={styles.valueItem}>{this.state.dataModel.products[0].nameModel}</Text>
                              </View>
                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={styles.titleItem}>Ngày kích hoạt</Text>
                                   <Text style={styles.valueItem}>{this._renderDateMonthYear(this.state.dataModel.createdDate)}</Text>
                              </View>
                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={styles.titleItem}>Mã kích hoạt</Text>
                                   <Text style={styles.valueItem}>{this.state.dataModel.guaranteeCode}</Text>
                              </View>
                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={styles.titleItem}>Serial cục nóng</Text>
                                   <Text style={styles.valueItem}>{this.state.dataModel.serial}</Text>
                              </View>
                              <View style={[BasicView.horizontalView, {}]}>
                                   <Text style={styles.titleItem}>Serial cục lạnh</Text>
                                   <Text style={styles.valueItem}>{this.state.dataModel.serial2}</Text>
                              </View>
                         </View>
                    )
               }
          }
     }
     render() {
       //   console.log(this.state.dataModel)
          const { dataModel } = this.state
          return (
               <Modal
                    style={styles.container}
                    animationType={"slide"}
                    visible={this.state.isShow}
                    transparent={true}
                    onRequestClose={() => this._setShow(false)}
               >
                    <View style={styles.outsideContainer} />
                    <View style={styles.content}>
                         <View style={styles.header}>
                              <Text style={styles.textTitle}>Chi tiết kích hoạt</Text>
                              <TouchableOpacity
                                   style={styles.onPressDelete}
                                   onPress={() => this._setShow(false)}
                              >
                                   <Image
                                        source={deleteIcon}
                                        style={styles.iconDelete}
                                        resizeMode={'center'}
                                   />
                              </TouchableOpacity>
                         </View>
                         { this._renderContent()}
                    </View>
               </Modal>
          );
     }
}
const screen = Dimensions.get('window');
const numberRadius = 5;
const styles = {
     container: {
          width: screen.width,
          height: screen.height,
          justifyContent: 'center',
          alignItems: 'center'
     },
     titleItem: {
          flex: 0.35,
          fontSize: 12,
          marginLeft: 20
     },
     valueItem: {
          flex: 0.65,
          fontSize: 12,
          marginLeft: 20
     },
     outsideContainer: {
          flex: 1,
          backgroundColor: '#000000',
          width: screen.width,
          opacity: 0.6,
          position: 'absolute',
          height: screen.height
     },
     header: {
          height: 50,
          width: '100%',
          backgroundColor: color.colorBlue,
          justifyContent: 'center',
          alignItems: 'center',
          borderTopLeftRadius: numberRadius,
          borderTopRightRadius: numberRadius,
          

     },
     textTitle: {
          color: color.colorWhite,
          fontSize: 15,
     },

     content: {
          width: '90%',
          height: '40%',
          backgroundColor: 'white',
          borderRadius: numberRadius,
          alignSelf: 'center',
          marginTop: '35%',


     },
     onPressDelete: {
          position: 'absolute',
          right: 0,
          width: 50,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center'
     },
     iconDelete: {
          width: 15,
          height: 15,
     },

}
export default ModalDetailActivation;
/**
 *  
 */