import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { DrawerNavigator } from 'react-navigation'; 
import ActivationHistory from './index';
import CustomerHistory from './CustomerHistory';
const HistoryNav = DrawerNavigator({
     ActivationHistory: { screen: ActivationHistory }
}, {
          drawerWidth: 300,
          drawerPosition: 'right',
          contentComponent: props => <CustomerHistory {...props} />
     })

export default HistoryNav;