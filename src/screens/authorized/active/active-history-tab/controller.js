export default class ActiveHistory {
      _employeeId = '';
      _startDate = 0;
      _endDate = 0;
      _responseListActivation = [];
      _customerId = '';
      _response = [];
      getResponseListActivation() {
            return this._responseListActivation;
      };
      getEmployeeId() {
            return this._employeeId;
      };
      getStartDate() {
            return this._startDate;
      };
      getEndDate() {
            return this._endDate;
      };
      setEmployeeId(id) {
            this._employeeId = id;
      };
      setStartDate(date) {
            this._startDate = date;
      };
      setEndDate(date) {
            this._endDate = date;
      };
      setResponseListActivation(value) {
            this._responseListActivation = value;
      };
      getCustomerId() {
            return this._customerId;
      };
      setCustomerId(id) {
            this._customerId = id;
      };
      getResponse() {
            return this._response;
      };
      setResponse(response) {
            this._response = response;
      };
      compare(a, b) {
            const genreA = a.createdDate;
            const genreB = b.createdDate;

            let comparison = 0;
            if (genreA > genreB) {
                  comparison = 1;
            } else if (genreA < genreB) {
                  comparison = -1;
            }
            //console.log(comparison)
            return comparison * -1;
      }
      async getListActivation(activation, cb) {
            cb.setState({ listLoading: true })
            // console.log('ABCCDAD', cb.props.currentUser, activation.getStartDate(), activation.getEndDate())
            await fetch((`https://app-dot-casper-electric.appspot.com/api?action=getActivation`), {

                  method: 'POST',
                  body: JSON.stringify({
                        userId: cb.props.currentUser,
                        fromDate: activation.getStartDate(),
                        toDate: activation.getEndDate(),
                  }),
                  headers: { "Content-Type": "application/json; charset=utf-8" }
            })
                  .then((response) => response.json())
                  .then((responJSON) => {
                        this.setResponseListActivation(responJSON.data.sort(this.compare))
                        //console.log('Ket qua tra ve', this.getResponseListActivation())
                        cb.setState({
                              isLoading: 0,
                              listLoading: false,
                              titleCheckDate: `Danh sách kích hoạt từ ${cb._renderDateMonthYear(listController.getStartDate())} đến ngày ${cb._renderDateMonthYear(listController.getEndDate())}`
                        })

                  })
                  .catch((error) => { console.log('nhay vao loi', error) })
                  ;
      }
      async _fetchHistory(cb) {
            cb.setState({ loadMenu: true })
            // console.log('ABCCDAD', cb.props.currentUser, activation.getStartDate(), activation.getEndDate())
            await fetch((`https://casper-electric.appspot.com/api?action=getActivationHistory`), {

                  method: 'POST',
                  body: JSON.stringify({
                        employeeId: cb.props.currentUser,
                        customerId: this.getCustomerId()

                  }),
                  headers: { "Content-Type": "application/json; charset=utf-8" }
            })
                  .then((response) => response.json())
                  .then((responJSON) => {
                        this.setResponse(responJSON.data)
                        console.log('Ket qua tra ve', responJSON.data)
                        cb.setState({ loadMenu: false })
                  })
                  .catch((error) => { console.log('nhay vao loi', error) })
                  ;
      }

}