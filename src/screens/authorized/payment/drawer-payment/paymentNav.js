import React, { Component } from 'react';
import { DrawerNavigator } from 'react-navigation';
import Payment from '../Payment';
import Option from './option';
const PaymentNav = DrawerNavigator({
     Payment: { screen: Payment}
}, {
     drawerWidth: 300,
     drawerPosition: 'right',
     contentComponent: props => <Option {...props} />
});
export default PaymentNav;
