import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, FlatList, ActivityIndicator, Platform } from 'react-native';
import { Toast } from 'native-base';
import { Text } from 'native-base';
import { connect } from 'react-redux';
import color from '../../../../ultis/styles/common-css';
import DatePicker from '../../../../components/date-picker';
import CustomPicker1 from '../../../../components/custom-picker';
import ListPayment from './list-payment';
import RevenueController from '../Controller';
import ModalLoading from '../../active/create-new-warranty/modal-loading';
import ButtonHotline from '../../../../components/button-hotline/ButtonHotline';

import Currency from 'currency.js';
import styles from './styles';
datafilter = [
    {
        id: -1,
        name: 'Tất cả'
    },
    {
        id: 0,
        name: 'Đã thanh toán'
    },
    {
        id: 1,
        name: 'Chưa thanh toán '
    }
]
const THRESHOLD = 0.5;
class ActiveHistory extends Component {
    constructor(props) {
        super(props);
        revenue = new RevenueController();
        this.state = {
            isLoading: true,
            errorMess: false,
            textResult: '',
            totalMoney: 0,
            hiddenOptimize: false,
            fromDate: '',
            toDate: '',
            moneyPaid: 0,
            statusChecked: -1,
            renderActivationOffset: true,
            isFetched: false,
            isLoadMore: true,
            data: [],
            activatedDevices: 0,
            noActivation: false,
            defaultValue: '',
            selectedPicker: true
        }
        page = 0;
        limit = 10;
    }
    async componentWillMount() {
        if(!this.props.isOnline) {
            this._showToast('Không có internet','danger','OK');
            return
        }
        await revenue.getActivationByOffset(page, limit, this.props.employeeId, this);
    }
    _toggleModalOption(visible) {
        this.ModalOption._setShow(visible)
    }
    _showToast(text, type,buttonText, onClose) {
    Toast.show({
      text: text,
      position: 'top',
      buttonText: buttonText,
      type: type,
      onClose: onClose
    })
  }
    // Render List StatisticActivation
    _renderItem = ({ item, index }) => {
        if (index === 0) {
            return (
                <View>
                    <View style={[styles.totalView]}>
                        <View style={{ flex: 0.3, borderBottomWidth: 0.5, borderBottomColor: color.colorWhite, borderRightColor: color.colorWhite, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={styles.textTotal}>Doanh số của {this.state.activatedDevices} lần kích hoạt </Text>
                        </View>
                        <View style={{ flex: 0.8, flexDirection: 'row' }}>
                            <View style={{ flex: 2, borderRightWidth: 0.5, borderRightColor: color.colorWhite, paddingLeft: 10, justifyContent: 'space-around' }}>
                                <Text style={styles.textTotal}>Từ: {this.state.fromDate}</Text>
                                <Text style={styles.textTotal}>Đến: {this.state.toDate}</Text>
                            </View>
                            <View style={{ flex: 3, justifyContent: 'space-around', padding: 10 }}>
                                <Text style={styles.textTotal}>Tổng : {Currency(this.state.totalMoney, { separator: ',', precision: 0 }).format()} VND </Text>
                                <Text style={styles.textTotal}>Nhận : {Currency(this.state.moneyPaid, { separator: ',', precision: 0 }).format()} VND </Text>
                                <Text style={styles.textTotal}>Thiếu : {Currency(this.state.totalMoney - this.state.moneyPaid, { separator: ',', precision: 0 }).format()} VND </Text>
                            </View>
                        </View>
                    </View>
                    <ListPayment
                        customerId={item.customerId}
                        moneyEmployeeRecent={item.moneyEmployeeRecent}
                        createdDate={item.createdDate}
                        employeeId={item.employeeId}
                        serial={item.serial}
                        serial2={item.serial2}
                        customerFullName={item.custommer.fullName}
                        customerPhoneNum={item.custommer.phoneNum}
                        status={item.status}
                    />

                </View>
            )
        }
        else {
            return (
                <ListPayment
                    customerId={item.customerId}
                    moneyEmployeeRecent={item.moneyEmployeeRecent}
                    createdDate={item.createdDate}
                    employeeId={item.employeeId}
                    serial={item.serial}
                    serial2={item.serial2}
                    customerFullName={item.custommer.fullName}
                    customerPhoneNum={item.custommer.phoneNum}
                    status={item.status}
                />
            )
        }
    }
    // Toggle ModalLoading
    _toggleModalLoading(visible) {
        this.ModalLoading._setShow(visible)
    }
    // convert date nhanh 
    // Render Load more
    _loadMore() {
        //console.log('loadmore', this.state.isLoadMore);
        //this.setState({ page: 1 });
        if (this.state.isLoadMore === true) {
            this.state.isLoadMore = false;
            page = page + limit;
            revenue.getMoreActivationByOffset(page, limit, this.props.employeeId, this);
        }
    }
    // Render Refresh
    _onRefresh() {

    }
    // Check picker date selected??? if no , disable button tuy chon
    _checkSelectedPicker() {
        if(!this.props.isOnline) {
            this._showToast('Không có internet','danger','OK');
            return
        }
        if (revenue.getFromDate() > revenue.getToDate()) {
            this._showToast('Ngày khởi đầu lớn hơn ngày kết thúc...','warning',"OK")
           // this.setState({ errorMess:  })
            return false
        }
        if (this.state.defaultValue === '') {
           this._showToast('Chưa chọn ngày tìm kiếm...','warning',"OK")
            return false
        }
        else {
            revenue.getStatisticByStatus(this.state.statusChecked, this); this.setState({ renderActivationOffset: false, selectedPicker: false });
            return true
        }
    }
    _checkData() {
        if(this.props.isFetching === true) {
            return<Text>Loading</Text>
        }
        else {
            this.setState({ isFetched: false, renderActivationOffset: false})
            return <Text>AA</Text>
        }   
    }
    render() {
        return (
            <View style={styles.container}>
                {
                    (this.state.hiddenOptimize == false) ? (
                        <TouchableOpacity
                            style={styles.showOptimize}
                            onPress={() => this.setState({ hiddenOptimize: true })}
                        >
                            <Text style={styles.textTotal}>Hiện tuỳ chọn</Text>
                        </TouchableOpacity>
                    ) : (
                            <View>
                                <View style={styles.viewDatePicker}>
                                    <DatePicker
                                        title={'Từ ngày...'}
                                        showIcon={false}
                                        getValue={(date) => { revenue.setFromDate(date); this.setState({ fromDate: revenue._convertFormatDate(revenue.getFromDate()), defaultValue: date }) }}
                                    />
                                    <DatePicker
                                        title={'Đến ngày...'}
                                        showIcon={false}
                                        getValue={(date) => { revenue.setToDate(date); this.setState({ toDate: revenue._convertFormatDate(revenue.getToDate()), defaultValue: date }) }}
                                    />
                                </View>
                                <View style={[styles.viewDatePicker]}>
                                    <CustomPicker1
                                        data={datafilter}
                                        textStyle={{ color: 'black' }}
                                        style={styles.viewPicker}
                                        getAddressCity={(value) => { this.setState({ statusChecked: value }) }}
                                        mode={'dropdown'}
                                    />
                                    <TouchableOpacity
                                        style={styles.searchButton}
                                        onPress={() => { this._checkSelectedPicker() }}
                                    >
                                        <Text style={{ color: color.colorWhite }}>Tra cứu</Text>
                                    </TouchableOpacity>

                                </View>
                                <View style={styles.viewDatePicker}>
                                    
                                    <TouchableOpacity
                                        style={[styles.showOptimize, { alignSelf: 'center', marginTop: 10, marginLeft: 0, width: '90%' }]}
                                        onPress={() => { this.setState({ hiddenOptimize: false, defaultValue: '', selectedPicker: true }) }}
                                    >
                                        <Text style={{ color: color.colorWhite }}>Ẩn tuỳ chọn</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        )
                }

                {
                    (this.state.isLoading === true) && (
                        <View style={{ justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center' }}>
                            <ActivityIndicator />
                            <Text>Đang lấy dữ liệu, vui lòng chờ...!</Text>
                        </View>
                    )
                }
                {
                    this.state.noActivation && <Text style={{ color: 'red', alignSelf: 'center', marginTop: 20 }}>Bạn chưa kích hoạt được sản phẩm nào...</Text>
                }
             
                {
                    (this.state.isFetched === true && this.state.renderActivationOffset === true) ?
                        (
                            <FlatList
                                data={revenue.getResponseStatisticByOffset()}
                                extraData={this.state}
                                keyExtractor={(item, index) => item.id}
                                renderItem={this._renderItem}
                                onEndReachedThreshold={THRESHOLD}
                                onEndReached={() => this._loadMore()}
                                refreshing={false}
                                onRefresh={() => this._onRefresh()}
                            />
                        ) : <FlatList
                            data={revenue.getResponseStatisticByStatus()}
                            extraData={this.state}
                            keyExtractor={(item, index) => item.id}
                            renderItem={this._renderItem}
                            onEndReachedThreshold={THRESHOLD}
                           // onEndReached={() => this._loadMore()}
                            refreshing={false}
                            onRefresh={() => this._onRefresh()}
                        />
                }
                    <ButtonHotline style={{ marginBottom: 10, marginTop: 10}} />
                <ModalLoading
                    ref={(ref) => this.ModalLoading = ref}
                />
            </View>
        );
    }
}
const mapStateToProps = state => {
    return {
        employeeId: state.getCurrentUser.data,
        isFetching: state.getList.isFetching,
        getList: state.getList.data,
        isOnline: state.getOnline.data,
    }
}
export default connect(mapStateToProps)(ActiveHistory);
