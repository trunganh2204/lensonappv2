const styles = {
    container: {
        flex: 1,
        backgroundColor: color.colorGray

    },
    text: {
        fontSize: 14,
        marginLeft: 20
    },
    textTotal: {
        color: color.colorWhite,

    },
    viewCustomPicker: {
        flexDirection: 'row',
        marginTop: 10,
        alignItems: 'center',

    },
    textTitle: {
        marginTop: 10,
        borderTopWidth: 0.5,
        width: '100%',
        height: 50,
        textAlign: 'left',
        textAlignVertical: 'center',
        color: 'black',
        borderBottomWidth: 0.5,
        paddingLeft: 10
    },
    searchButton: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: '40%',
        backgroundColor: color.colorBlue,
        borderRadius: 4,

    },
    viewDatePicker: {
        width: '100%',
        justifyContent: 'space-around',
        marginTop: 10,
        flexDirection: 'row',

    },
    viewPicker: {

        //borderRadius: 4,
        width: 145,
        height: 40,
        //backgroundColor: color.colorBlue,

    },
    totalView: {
        width: '90%',
        height: 150,
        backgroundColor: color.colorBlue,
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,

    },
    showOptimize: {
        width: '90%',
        height: 50,
        backgroundColor: color.colorBlue,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    }
}
export default styles;