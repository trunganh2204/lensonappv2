import React, { Component } from 'react';
import { View } from 'react-native';
import { Text } from 'native-base';
import BasicView from '../../../../../ultis/styles/AppStyles';
import color from '../../../../../ultis/styles/common-css';
import currency from 'currency.js';
class ListPayment extends Component {
     convertDate(value) {
          let temp = new Date(value)
          return `${temp.getDate()}/${temp.getMonth() + 1}/${temp.getFullYear()}` 
     }
     render() {
          return (
               <View style={styles.container}>
                    <View style={[BasicView.horizontalView]}>
                         <Text style={styles.titleItem}>Khách hàng</Text>
                         <Text style={styles.valueItem}>{this.props.customerFullName}</Text> 
                    </View>
                    <View style={[BasicView.horizontalView]}>
                         <Text style={styles.titleItem}>Số điện thoại</Text>
                         <Text style={styles.valueItem}>{this.props.customerPhoneNum}</Text>  
                    </View>
                    <View style={[BasicView.horizontalView]}>
                         <Text style={styles.titleItem}>Nhân viên</Text>
                         <Text style={styles.valueItem}>{this.props.employeeId}</Text>
                    </View>
                    
                    <View style={[BasicView.horizontalView]}>
                         <Text style={styles.titleItem}>Ngày kích hoạt</Text>
                         <Text style={styles.valueItem}>{this.convertDate(this.props.createdDate)}</Text>
                    </View>
                    <View style={[BasicView.horizontalView]}>
                         <Text style={styles.titleItem}>Tiền hoa hồng</Text>
                         <Text style={[styles.valueItem,{color: 'red'}]}> {currency(this.props.moneyEmployeeRecent, { separator: ',', precision: 0 }).format()} VND</Text>
                    </View>
                    <View style={[BasicView.horizontalView]}>
                         <Text style={styles.titleItem}>Trạng thái</Text>
                       {
                            (this.props.status ==0) ?  <Text style={[styles.valueItem,{color: 'red'}]}>Đã thanh toán</Text> : <Text style={[styles.valueItem,{color: 'red'}]}>Chưa thanh toán</Text>
                       } 
                    </View>
               </View>

          );
     }
}

const styles = {
     container: {
          width: '90%',
          height: 200,
          alignSelf: 'center',
          borderWidth: 1,
          justifyContent: 'space-around',
          paddingLeft: 10,
          backgroundColor: '#fff',
          borderRadius: 2,
          borderColor: '#ddd',
          borderBottomWidth: 0,
          shadowColor: '#000',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.8,
          shadowRadius: 2,
          elevation: 1,
          marginTop: 10
          
     },
     titleItem: {
          flex: 0.45,
          alignItems: 'center'
     },
     valueItem: {
          flex: 0.55,
          alignItems: 'center'
     }
}
export default ListPayment;