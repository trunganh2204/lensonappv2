const baseURL = 'https://app-dot-casper-electric.appspot.com/api?';
const action = 'action=';
export const actionOfAPI = {
     GET_CATEGORY: 'getCategorys',
     GET_PRODUCT: 'getProduct',
     GET_TRANSACTION: 'getTransaction',
     SEARCH: 'search',
     GET_PRODUCT_IN_CATEGORY: 'getProductsByCategoryId',
     GET_APP_DATA: 'getAppdata',
     SAVE_CUSTOMER: 'signUp',
     //SEARCH_CUSTOMER: 'search'

}
export const convertURL = (actionType, params, status) => {
     switch(actionType) {
          case actionOfAPI.GET_CATEGORY: {
               return baseURL + action + actionOfAPI.GET_CATEGORY  + params + '&status=' + status;
          };
          case actionOfAPI.GET_PRODUCT_IN_CATEGORY: {
               return baseURL + action + actionOfAPI.GET_PRODUCT_IN_CATEGORY+ '&categoryId=' + params+ '&status=' + status
          };
          case actionOfAPI.GET_APP_DATA: {
               return baseURL + action + actionOfAPI.GET_APP_DATA
          };
          case actionOfAPI.SEARCH: {
               return baseURL + action + actionOfAPI.SEARCH + '&keyword=' + params + '&typeIndexName=' + status;
          };
          case actionOfAPI.SAVE_CUSTOMER : {
               return baseURL + action + actionOfAPI.SAVE_CUSTOMER
          };
          default: return 'Code sai cmnr'
     }
};



