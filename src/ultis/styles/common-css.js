export default color = {
     backgroundColor: '#fff',
     activeButtonColor: '#E8681C',
     defaultTextColor: '#4493AA',
     defaultBackgroundColor: '#F1F1F1',
     // Theo mau`
     colorBlue: '#4493AA',
     colorWhite: '#fff',
     colorOrange: '#E8681C',
     colorGray: '#F1F1F1'

}