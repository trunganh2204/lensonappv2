import { NetInfo } from 'react-native';
export default class CheckInterNet {
    isOnline(callBack) {
        NetInfo.isConnected.fetch().then(isConnected => {
            callBack(isConnected)
        })
    }

}